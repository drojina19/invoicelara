<?php

namespace App\Constant;

class Constant{
    const DELETE_SOFT="Are You Sure, Want To Delete???";
    const DELETE_PERMANENT="Are You Sure, Want To Delete Permanently???";
    const RESTORE_ITEM="Are You Sure, Want To Restore???";
    const GST10="GTS10";
    const GST_10="GTS -10%";
    const GSTINCLUDED="IncludingGST";
    const GST_INCLUDED="Including GST";
    const DE_ACTIVATE="Are You Sure, Want To De-activate???";
    const ACTIVATE="Are You Sure, Want To Activate???";
    const INVOICE_SYSTEM="Invoice System";

}
