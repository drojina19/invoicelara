<?php

namespace App;

use App\Model\Permission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address','mobile','gender','image','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany(Model\Role::class);
    }
    public function hasPermission(Permission $permission){
        $roles=$permission->roles;
        $rolesCount=$roles->intersect($this->roles)->count();
        //dd($rolesCount);
        return $rolesCount;
    }
}
