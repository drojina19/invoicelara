<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable=['supplier_name','abn','website','logo','phone1','phone2','email','street','city','state','zip_code','country','status'];


    /*
     * payment method for the suppliers
     * defining relationship
     *
     * */
    public function payments()
    {
       return $this->hasMany('App\Model\Payment');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
