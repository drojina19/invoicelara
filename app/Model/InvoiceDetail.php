<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceDetail extends Model
{
    use SoftDeletes;
    protected $fillable = ['invoice_id','invoice_no', 'product_id', 'quantity', 'rate', 'amount', 'description'];
    protected $dates=['deleted_at'];
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
//    public function payment()
//    {
//        return $this->belongsTo(Payment::class);
//    }
}
