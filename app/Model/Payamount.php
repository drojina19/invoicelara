<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payamount extends Model
{
    use SoftDeletes;
    protected $fillable = ['paid_date','paid_amount','invoice_id','invoice_no'];
    protected $dates=['deleted_at'];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function setPaidDateAttribute($value){
        $this->attributes['paid_date']=Carbon::createFromFormat(config('app.dateFormat'),$value);
    }

}
