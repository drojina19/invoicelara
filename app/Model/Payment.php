<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Payment extends Model
{
    use SoftDeletes;
    protected $fillable=['supplier_id','pay_method','bank_name','bsb','account_no','status'];
    protected $dates=['deleted_at'];

    /*
     * 
     * creating relationship with supplier
     * 
     */
    public function supplier()
    {
        return $this->belongsTo('App\Model\Supplier');
    }
    public  function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
}
