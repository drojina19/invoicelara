<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable=['name','status'];

    protected $dates=['deleted_at'];

    public function invoiceDetails(){
        return $this->hasMany(InvoiceDetail::class);
    }

    /**
    * Override parent boot and Call deleting event
    *
    * @return void
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($products) {
            if ($products->forceDeleting) {
                $products->invoiceDetails()->withTrashed()->get()
                    ->each(function($inv_dtl) {
                        $inv_dtl->forceDelete();
                    });
            } else {
                $products->invoiceDetails()->delete();
            }
        });

        static::restoring(function($products){
            $products->invoiceDetails()->withTrashed()->get()
                ->each(function($inv_dtl) {
                    $inv_dtl->restore();
                });
        });

    }
}
