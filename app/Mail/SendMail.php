<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;
class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $invoice;
    public function __construct($invoice)
    {
        $this->invoice=$invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        PDF::loadView('admin.invoice.extraview.pdf',array('invoice'=>$this->invoice))->save($this->invoice->invoice_no.'-invoice.pdf');
        return $this->attach($this->invoice->invoice_no.'-invoice.pdf')
                    ->from($this->invoice->supplier->email)
                    ->bcc('invoice@geniusitsolutions.com.au')
                    ->view('mail.sendinvoicepdf');
    }
}
