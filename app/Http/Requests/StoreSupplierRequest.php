<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->segment(2));
        return [
            'supplier_name'=>"required",
            'email'=>'required|email',
            'abn'=> "required",
            'phone1'=> "required",
            'street'=> "required",
            'city' => "required",
            'state'=> "required",
            'country'=> "required",
            'zip_code'=> "required"
            
        ];
    }
}
