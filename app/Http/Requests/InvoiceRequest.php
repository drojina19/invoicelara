<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd(Input::all());
        return [
            'supplier_id'=> 'required',
            'customer_id'     => 'required',
            'issue_date'   => 'required',
            'due_date'     => 'required',
//            'invoice_no'   => 'required|unique:invoices',
        ];
    }
}
