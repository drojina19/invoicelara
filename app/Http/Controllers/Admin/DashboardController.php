<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PasswordChangeRequest;
use App\Model\Dailyrecord;
use App\Model\Invoice;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index(){
        $invoices=Invoice::get();
        return view('admin.dashboard')
            ->with('invoices',$invoices);
    }

    public  function  changePassword()
    {
        return view('admin.changepassword');
    }

    public  function  updatePassword(PasswordChangeRequest $request)
    {
        if(Hash::check(Input::get('old_password'), Auth::user()->password)){
            $user=User::where('id',Auth::user()->id)->first();
            $user->password=bcrypt(Input::get('new_password'));
            $user->save();
            Session::flash('psd',"New password is updated successfully");
        }else{
            Session::flash('psd',"Old password does not match the record");
        }
        return redirect()->back();
    }
//    public function truncatedailyrecord(){
//        Dailyrecord::truncate();
//        return view('admin.dashboard');
//    }
}
