<?php

namespace App\Http\Controllers\Admin;
/*
 * coded by sagar kc
 * date: wed aug 9 2017
 *
 * */
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\Customer;

use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /*
     *
     * index page redirect making variable to use
     * */
    public function index()
    {
        $customers=Customer::orderBy('id','DESC')->get();
        return view('admin.customer.index')
            ->with('customers',$customers);

    }

    /*
     * show the data individually through id*/
    public function show($id)
    {
        $customer=Customer::where('id',$id)->first();
        return view('admin.customer.detail')
            ->with('customer',$customer);

    }

    /*create page */
    public function create()
    {
        return view('admin.customer.create');
    }


    /*storing the data and images in database*/
    public function store(CustomerRequest $request)
    {
        $customer=Customer::create(Input::all());
        if (Input::hasFile('image'))
        {
            $image= Input::File('image');
            $imageName=\Illuminate\Support\Str::random('32').".".$image->getClientOriginalExtension();
            $image=move('images/customer',$imageName);
            $customer->image=$imageName;
        }
        $customer->save();
        Session::flash('customer','Customer Added Successfully');
        return redirect('customer');


    }


    /*editing the data through id and redirect*/
    public function edit($id)
    {
        $customer=Customer::where('id',$id)->first();

        return view('admin.customer.edit')
            ->with('customer',$customer);
    }



    /*updating the data and images  which has been updated using id*/
    public function update(CustomerRequest $request, $id)
    {
        $customer=Customer::find($id);
        $customer->update(Input::all());
        if(Input::hasFile('image')){
            $image=  Input::file('image');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/customer',$imageName);
            $customer->image=$imageName;
        }
        $customer->save();
        Session::flash('customer','Customer Updated Successfully');
        return redirect('customer');

    }

    /*deleting the data using id (soft delete) */
    public function destroy($id)
    {
        $customer=Customer::find($id);
        $customer->delete();
        Session::flash('customer','Customer Deleted Successfully');
        return redirect('deletedcustomers');
    }

    /*send the deleted data to trash for further use(not deleting directly)*/
    public function deletedCustomer(){
        $customers=Customer::onlyTrashed()->get();
        return view('admin.customer.deletedcustomer')->with('customers',$customers);


    }

    /*restoing the data which has been deleted using id and redirect to index page*/
    public function restoreCustomer($customerid){
        $customer=Customer::onlyTrashed()->find($customerid);
        $customer->restore();
        Session::flash('customer','Customer Restored Successfully');
        return redirect(route('customer.index'));
    }
    /*
     * hard delete customer id directlu
     * */
    public function forcedeletecustomer($customerid){
        $customer=Customer::onlyTrashed()->find($customerid);
        $customer->forceDelete();
        Session::flash('customer','Customer Permanently Deleted Successfully');
        return redirect('deletedcustomers');
     }
}
