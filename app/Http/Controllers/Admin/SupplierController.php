<?php

namespace App\Http\Controllers\Admin;
/*
 * coded by:rojina 
 * date:8th august
 *
 * */

use App\Http\Requests\StoreSupplierRequest;
use App\Model\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class SupplierController extends Controller
{
//    private $supplierRepo;
//    public function  __construct(\SupplierRepository $supplierRepo){
//        $this->supplierRepo=$supplierRepo;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $suppliers=$this->supplierRepo->index();
        $suppliers=Supplier::orderBy('id','DESC')->get();
       return view('admin.supplier.index',compact('suppliers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSupplierRequest $request)
    {

//        $this->supplierRepo->store($request->toArray());

        $supplier=Supplier::create(Input::all());
        if(Input::hasFile('logo')){
            $image=  Input::file('logo');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/supplier',$imageName);
            $supplier->logo=$imageName;
        }
        Session::flash("suplier","New Supplier Added Successfully");
        $supplier->save();
        return redirect('/supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier=Supplier::where('status',1)->where('id',$id)->first();
        return view('admin.supplier.detail')
            ->with('supplier',$supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier=Supplier::where('id',$id)->first();
        return view('admin.supplier.edit')
            ->with('supplier',$supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSupplierRequest $request, $id)
    {
        $supplier=Supplier::find($id);
        $supplier->update(Input::all());
//        dd($supplier);
        if(Input::hasFile('logo')){
            $image=  Input::file('logo');
            $imageName=  \Illuminate\Support\Str::random('32').'.'.$image->getClientOriginalExtension();
            //now save the image file
            $image->move('images/supplier',$imageName);
            $supplier->logo=$imageName;
        }
        $supplier->save();
        Session()->flash("supplier","Supplier Update Successfully");
        return redirect('/supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier=Supplier::find($id);
        $supplier->delete();
        Session()->flash("supplier","Supplier Deleted Successfully");
        return redirect('/supplier');
    }
}
