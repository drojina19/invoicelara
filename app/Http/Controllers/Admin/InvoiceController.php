<?php

namespace App\Http\Controllers\Admin;
/*
 *
 * coded by:Rojina
 * date:15th August
 *
 *
 */

use App\Http\Requests\InvoiceRequest;
use App\Model\Invoice;
use App\Model\InvoiceDetail;
use App\Model\Payment;
use App\Model\Product;
use App\Model\Supplier;
use App\Model\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use PDF;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::orderBy('id', 'DESC')->get();
        $customers = Customer::get();
        $suppliers = Supplier::get();
        return view('admin.invoice.index', compact('customers', 'suppliers'))
            ->with('invoices', $invoices);

    }

    /**
     * Show the form for creating a new invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::where('status', 1)->get();
        $customers = Customer::where('status', 1)->get();
        $products = Product::where('status', 1)->get();
        return view('admin.invoice.create')
            ->with('suppliers', $suppliers)
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('invoiceno', $this->rowcount());
    }

    /**
     * Store a newly created invoice in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(InvoiceRequest $request)
    {
        $payment_id = $this->getPaymentMethodId(Input::get('supplier_id'));
        if ($payment_id==0){
            Session::flash('payment',"First add the pay method for this supplier");
            return redirect('payment/add/'.Input::get('supplier_id'));
        }
//        dd(Input::all());
        DB::transaction(function () use(&$payment_id) {
            //get payment id

            // invoice number genereated
            $invoiceno = strtoupper(substr($this->getSupplierDetail(Input::get('supplier_id')), 0, 3)) . "-INV" . Input::get('invoice_no');

            // invoice saved in invoice table
            $invoice = Invoice::create([
                'invoice_no' => $invoiceno,
                'supplier_id' => Input::get('supplier_id'),
                'customer_id' => Input::get('customer_id'),
                'payment_id' => $payment_id,
                'issue_date' => Input::get('issue_date'),
                'due_date' => Input::get('due_date'),
                'total' => Input::get('total'),
                'gst_type' => Input::get('gst_type'),
                'gst' => Input::get('gst'),
                'grandtotal' => Input::get('grandtotal'),
                'amt_owed' => Input::get('grandtotal')
            ]);

            // part to save data in invoice_detail table
            $p_ids = Input::get('product_id');
            for ($i = 0; $i < count($p_ids); $i++) {
                InvoiceDetail::create([
                    'invoice_id' => $invoice->id,
                    'invoice_no' => $invoice->invoice_no,
                    'product_id' => Input::get('product_id')[$i],
                    'quantity' => Input::get('quantity')[$i],
                    'rate' => Input::get('rate')[$i],
                    'amount' => Input::get('amount')[$i],
                    'description' => Input::get('description')[$i]
                ]);
            }


            Session()->flash('invoice', "Added Successfully");
        });
        return redirect('/invoice');
    }


    /*
     * get active payment method id of supplier
     * */
    public function getPaymentMethodId($supplier_id)
    {
        $pay_method = Payment::where('status', 1)->where('supplier_id', $supplier_id)->first();
        if (count($pay_method)>0){
            return $pay_method->id;
        }
        return 0;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($invoice_id)
    {
        $invoice = Invoice::where('id', $invoice_id)->first();
        if (count($invoice)==0){
            return abort(404);
        }
        return view('admin.invoice.invoicedetail')
            ->with('invoice', $invoice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::where('status', 1)->get();
        $customers = Customer::where('status', 1)->get();
        $products = Product::where('status', 1)->get();
        $invoice = Invoice::where('id', $id)->first();
        return view('admin.invoice.edit')
            ->with('suppliers', $suppliers)
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('invoice', $invoice);
    }

    /**
     * Update the specified invoice in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd(Input::all());
        DB::transaction(function () use ($id) {
            $invoice = Invoice::find($id);
            $invoice->update(Input::all());
            $invoice->amt_owed = Input::get('grandtotal');
            $invoice->save();
            InvoiceDetail::where('invoice_id', $id)->forceDelete();
            $p_ids = Input::get('product_id');
            for ($i = 0; $i < count($p_ids); $i++) {
                InvoiceDetail::create([
                    'invoice_id' => $invoice->id,
                    'invoice_no' => $invoice->invoice_no,
                    'product_id' => Input::get('product_id')[$i],
                    'quantity' => Input::get('quantity')[$i],
                    'rate' => Input::get('rate')[$i],
                    'amount' => Input::get('amount')[$i],
                    'description' => Input::get('description')[$i]
                ]);
            }
            Session()->flash('invoice', "Updated Successfully");
        });
        return redirect('/invoice');
    }

    /**
     * Remove the specified invoice  from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $invoice = Invoice::find($id);
            $invoice->delete();
            InvoiceDetail::where('invoice_id', $id)->delete();
            Session()->flash('invoice', "Deleted Successfully!!!");
        });
        return redirect('/deletedinvoices');
    }

    public function deletedInvoices()
    {
        $deletedinvoices = Invoice::onlyTrashed()->get();
        return view('admin.invoice.deletedinvoices', compact('deletedinvoices'));
    }

    /*
     * count the row of invoice table
     * */
    public function rowcount()
    {
        $invoice = Invoice::orderBy('id', 'desc')->first();
        if (count($invoice) < 1) {
            return "01";
        } else {
            $count = $invoice->id + 1;
            if ($count > 10) {
                return $count;
            } else {
                return "0" . $count;
            }
        }

    }

    /*To get supplier details*/
    public function getSupplierDetail($supplier_id)
    {
        $supplier = Supplier::where('id', $supplier_id)->first();
        return $supplier->supplier_name;
    }

    /*To get deleted invoice information*/
    public function showdeletedInvoice($invoice_id)
    {

        $invoice = Invoice::where('id', $invoice_id)->onlyTrashed()->first();
        $invdetails = InvoiceDetail::onlyTrashed()->where('invoice_id', $invoice_id)->get();
        return view('admin.invoice.deletedinvoiceinfo')
            ->with('invoice', $invoice)
            ->with('invdetails', $invdetails);

    }
    /*ends here*/

    /*To restore deleted invoice */
    public function restoreDeletedInvoice($invoice_id)
    {
        $invoice = Invoice::onlyTrashed()->find($invoice_id);
        $invoice->restore();
        $invdetails = InvoiceDetail::onlyTrashed()->where('invoice_id', $invoice_id)->restore();
//        $invdetails->restore();
        Session()->flash('invoice', "Deleted Invoice Restored Successfully ");
        return redirect('/deletedinvoices');

    }
    /*Ends here*/

    /*To  delete permanently*/
    public function forceDeleted($invoice_id)
    {
        $invoice = Invoice::onlyTrashed()->find($invoice_id);
        $invoice->forceDelete();
        $invdetails = InvoiceDetail::onlyTrashed()->where('invoice_id', $invoice_id)->forceDelete();
        Session()->flash('invoice', "Deleted Invoice Permanently Successfully");
        return redirect('/deletedinvoices');

    }
    /*Ends here*/


    /*
     * print invoice method
     * */
    public function printInvoice($invoice_id)
    {
        $invoice = Invoice::where('id', $invoice_id)->first();
        return view('admin.invoice.extraview.print', compact('invoice'));
    }

    /*For  pdf generator*/
    public function generatePdf($invoice_id)
    {
        $invoice = Invoice::where('id', $invoice_id)->first();
//        if ($request->has('download')) {
            $pdf = PDF::loadView('admin.invoice.extraview.pdf', compact('invoice'));
            return $pdf->download($invoice->invoice_no.'-invoice.pdf');


    }

    /*
     * send mail to customer with invoice attached
     * */
    public function sendInvoiceMail($invoice_id)
    {
        $invoice = Invoice::where('id', $invoice_id)->first();
        Mail::to($invoice->customer->email)->send(new \App\Mail\SendMail($invoice));
//        if($mail_i){
            unlink($invoice->invoice_no."-invoice.pdf");
//        }
        return redirect()->back()->with('invoice','Email has been sent successfully');

    }
}