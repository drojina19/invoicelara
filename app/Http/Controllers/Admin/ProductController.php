<?php

namespace App\Http\Controllers\Admin;

/*
 * coded by:Rojina
 * date    :13th August
 *
 *
 */

use App\Http\Requests\ProductRequest;
use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rules\In;
use Illuminate\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the product.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::orderBy('id',"DESC")->get();
        return view('admin.product.index')
            ->with('products',$products);
    }

    /**
     * Show the form for creating a new product.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        for($i=0;$i<count(Input::get('name'));$i++)
        {
            if(Input::get('name')[$i]!='' || (Input::get('name')[$i]!=null)){
                Product::create([
                    'name'=>Input::get('name')[$i]
                ]);
            }
        }
        Session()->flash('product',"Added Product Successfully");
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::where('id',$id)->first();
        return view('admin.product.edit')
            ->with('product',$product);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product=Product::find($id);
        $product->update(Input::all());
        Session()->flash('product',"Updated Products Successfully");
        return redirect('/product');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        Session()->flash('product',"Deleted Products Successfully");
        return redirect('deletedproducts');
    }

    /* send the deleted data to trash for further use(not deleting directly)*/

    public function deletedProduct()
    {
       $products=Product::onlyTrashed()->get();
        return view('admin.product.deletedproduct')
            ->with('products',$products);

    }
    /*
     *
     *
     * Restore the products that were deleted previously
     */

    public function restoreProduct($productid)
    {
        $product=Product::onlyTrashed()->find($productid);
        $product->restore();
        Session()->flash('product',"Restored Product Successfully");
        return redirect('/deletedproducts');

    }
    /*
     * to delete permanently from the list of products that were soft deleted
     *
     */

    public  function forceDelete($productid)
    {
        $product = Product::onlyTrashed()->find($productid);
        $product->forceDelete();
        Session()->flash('product', "Permanently Deleted Successfully !!!");
        return redirect('/deletedproducts');
    }

   public function productEnable($id)
   {
       $product=Product::find($id);
       $product->status=1;
       $product->save();
       Session()->flash('product',"Choosen Product Activated");
       return redirect('/product');

   }
    public  function productDisable($id)
    {
        $product=Product::find($id);
        $product->status=0;
        $product->save();
        Session()->flash('product',"Choosen Product Deactivated");
        return redirect('/product');
    }
}
