<?php

namespace App\Http\Controllers\Admin;

use App\Model\Invoice;
use App\Model\Payamount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PayamountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payamounts=Payamount::get();
        return view('admin.payamount.index')
            ->with('payamounts',$payamounts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enterAmount($invoice_id)
    {
        $invoice=Invoice::where('id',$invoice_id)->first();
        if (count($invoice)==0){
            return abort(404);
        }
        return view('admin.payamount.create', compact('invoice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd(Input::all());
        DB::transaction(function(){
            $payamount=Payamount::create([
                    'invoice_no'=>Input::get('invoice_no'),
                    'invoice_id'=>Input::get('invoice_id'),
                    'paid_date'=>Input::get('paid_date'),
                    'paid_amount'=>number_format((float)Input::get('paid_amount'), 2, '.', '')
                ]);
            $invoice=Invoice::find(Input::get('invoice_id'));
            $_new_amt_owed=$invoice->amt_owed-$payamount->paid_amount;
            $new_amt_owed=number_format((float)$_new_amt_owed, 2, '.', '');
            $invoice->amt_owed=$new_amt_owed;
            if($new_amt_owed==0){
                $invoice->status=1;
            }
            $invoice->save();
            Session()->flash('payamount',"Added New Payamount Successfully");
        });
        return redirect('/payamount/'.Input::get('invoice_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($invoice_id)
    {
//        $payamount=Payamount::where('invoice_id',$invoice_id)->get();
        $invoice=Invoice::where('id',$invoice_id)->first();
        if (count($invoice)==0){
            return abort(404);
        }
        return view('admin.payamount.index')
            ->with('invoice',$invoice);
//            ->with('payamount',$payamount);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payamount=Payamount::where('id',$id)->first();

        return view('admin.payamount.edit')
            ->with('payamount',$payamount);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payamount=Payamount::find($id);
        $payamount->update(Input::all());
        $payamount->save();
        Session()->flash('payamount',"Updated Payamount Successfully");
        return redirect('/payamount');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice_id=null;
        DB::transaction(function() use($id, &$invoice_id){
            $payamount=Payamount::find($id);
            $paid_amount=$payamount->paid_amount;
            $invoice_id=$payamount->invoice_id;
            if($payamount->delete()){
                $invoice=Invoice::find($invoice_id);
                $new_amt_owed=$invoice->amt_owed+$paid_amount;
                $invoice->amt_owed=number_format((float)$new_amt_owed, 2, '.', '');
                $invoice->status=0;
                $invoice->save();
            }
            Session()->flash('payamount',"Deleted Payamount Successfully");
        });
        return redirect('/payamount/'.$invoice_id);
    }

    
}
