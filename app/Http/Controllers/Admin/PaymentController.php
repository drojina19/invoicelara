<?php

namespace App\Http\Controllers\Admin;
/*
 * Code By: Rojina
 * Date: 10 aug 2017
 * */
use App\Http\Requests\PaymentRequest;
use App\Model\Payment;
use App\Model\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


class PaymentController extends Controller
{
    /**
     * Display a list of the supplier's payment methods.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $payments=Payment:: OrderBy('id','DESC')->get();
//        return view('admin.payment.index')
//            ->with('payments',$payments);

    }
//
    /**
     * Show the form for creating a new payment method.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.payment.create');
    }

    /**
     * Store a newly created payment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentRequest $request)
    {
        $payment=Payment::create(Input::all());
        $payment->save();
        Payment::where('id','!=',$payment->id)->update(['status'=>0]);
        Session::flash('payment','New Payment Method Added Successfully');
        return redirect('/payment/'.Input::get('supplier_id'));

    }
    /*
     * Creating form  regarding payment method based on supplier_id
     *@param int $supplier
     * @@return \Illuminate\Http\Response
     *
     *
     */

    public function addInfo($supplier_id)
    {
        $supplier = Supplier::where('id', $supplier_id)->first();
        return view('admin.payment.create')
            ->with('supplier', $supplier);
    }

    /**
     * Display the specified payment method for specified supplier.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @param int $supplier
     */
    public function show($supplier_id)
    {
        $supplier =Supplier::where('id',$supplier_id)->first();
        $payments=Payment::where('supplier_id',$supplier_id)->orderBy('id','DESC')->get();
        return view('admin.payment.index')
            ->with('payments',$payments)
            ->with('supplier',$supplier);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment=Payment::where('id',$id)->first();
        $supplier=Supplier::where('id',$payment->supplier_id)->first();
        return view('admin.payment.edit')
            ->with('payment',$payment)
            ->with('supplier',$supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentRequest $request, $id)
    {
        $payment=Payment::find($id);
//        $supplier=Supplier::where('id',$payment->supplier_id)->first();
        $payment->update(Input::all());
        $payment->save();
        Session()->flash('payment',"Updated Payment Method Successfully");
        return redirect('/payment/'.$payment->supplier_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment=Payment::find($id);

        $payment->delete();
        Session()->flash('payment',"Deleted Payment Method Successfully");
        return redirect('/deletedpayment/'.$payment->supplier_id);

    }

    public function mtdEnable($p_mtd_id){
        $payment=Payment::find($p_mtd_id);
        $payment->status=1;
        $payment->save();
        Payment::where('id','!=',$p_mtd_id)->update(['status'=>0]);
        Session()->flash('payment',"Choosen Payment Method Activated Successfully");
        return redirect('/payment/'.$payment->supplier_id);

    }
    /*
     *
     * Supplying supplierid and deleting Payment methods from the associated suppliers
     */

    public function deletedPayMethod($supplierid){
        $payments=Payment::where('supplier_id',$supplierid)->onlyTrashed()->get();
        $supplier=Supplier::where('id',$supplierid)->first();
        return view('admin.payment.deleted', compact('payments','supplier'));
    }
    /*
     *
     *
     * For restoring the payment methods once deleted
     */
    public function restorePayMethod($p_mtd_id)
    {
        $payment=Payment::onlyTrashed()->find($p_mtd_id);
        $payment->restore();
        Session()->flash('payment',"Restore Payment Method Successfully");
        return redirect('/deletedpayment/'.$payment->supplier_id);
    }

    /*
     *
     *
     * For deleting the payment methods permanantly
     */
    public function forceDelete($p_mtd_id)
    {
        $payment=Payment::onlyTrashed()->find($p_mtd_id);
        $payment->forceDelete();
        Session()->flash('payment',"Payment Method Permanently Deleted Successfully!!!!");
        return redirect('/deletedpayment/'.$payment->supplier_id);
    }
}
