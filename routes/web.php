<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Auth::routes();

Route::get('/home', function(){
    return redirect('/');
});
Route::group(['middleware'=>['auth']],function(){
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('/dashboard', 'Admin\DashboardController@index');

    /*
     * Sagar
     **/

    Route::resource('/customer','Admin\CustomerController');//customer's main page
    Route::get('/forcedeletecustomer/{customerid}','Admin\CustomerController@forcedeletecustomer');//customer's hard delete
    Route::get('/deletedcustomers','Admin\CustomerController@deletedCustomer');//customer deleted list page
    Route::get('/restorecustomer/{customerid}','Admin\CustomerController@restoreCustomer');//restoreing the deleted data using id from restore page to index page

    /*
     * end sagar code
     **/


    /*
     * rojina
     **/

    Route::resource('/supplier','Admin\SupplierController');//displays supplier's landing page

    /* Payment started*/
    Route::resource('/payment','Admin\PaymentController');//displays payment landing page

    Route::get('/payment/add/{supplier}','Admin\PaymentController@addInfo');// For listingform  regarding payment method based on supplier_id

    Route::get('/payment/mtdenable/{supplier}','Admin\PaymentController@mtdEnable');// For listing enabling payment method by passing supplier's id

    Route::get('/deletedpayment/{supplier}','Admin\PaymentController@deletedPayMethod');// For  listing temporarily deleted  payment method of the given supplier

    Route::get('/restorepayment/{paymentid}','Admin\PaymentController@restorePayMethod');//To  listing restored  payment method details once deleted//

    Route::get('/forcedeletepaymtd/{paymentid}','Admin\PaymentController@forceDelete');//To delete the payment method that is soft deleted once

    /* Payment ended*/
    /*Invoice start*/

    Route::resource('/invoice','Admin\InvoiceController');//displays invoice's landing page
    Route::get('/deletedinvoices','Admin\InvoiceController@deletedInvoices');//invoice deleted page
    Route::get('/deletedinvoicesinformation/{invoiceid}','Admin\InvoiceController@showdeletedInvoice');//Invoice deleted detail page
    Route::get('/restoredeletedinvoice/{invoiceid}','Admin\InvoiceController@restoreDeletedInvoice');//To restore deleted invoice
    Route::get('/forcedeletedinvoice/{invoiceid}','Admin\InvoiceController@forceDeleted');//To delete invoice permanently
    Route::get('/paidinvoices','Admin\InvoiceController@index');

    /*Invoice ended*/

    /*Product started*/
    Route::resource('/product','Admin\ProductController');//displays product main page
    Route::get('/deletedproducts','Admin\ProductController@deletedProduct');//product deleted page
    Route::get('/restoreproduct/{productid}','Admin\ProductController@restoreProduct');//For restoring the deleted products soft deleted  before
    Route::delete('/forcedeletedproduct/{productid}','Admin\ProductController@forceDelete');//For deleting product permanently
    Route::get('/product/p-enable/{productid}','Admin\ProductController@productEnable');//To enable product status that was disabled before
    Route::get('/product/p-disable/{productid}','Admin\ProductController@productDisable');//To disable product status that was enabled before

    /*Product ended*/

    /*Payamount Started*/
    Route::resource('/payamount','Admin\PayamountController');
    Route::get('/addpayamount/{invoiceid}','Admin\PayamountController@enterAmount');


    /* print invoice */
    Route::get('print-invoice/{invoiceid}', "Admin\InvoiceController@printInvoice");

    /*pdf generator*/
    Route::get('/downloadpdf/{invoiceid}',"Admin\InvoiceController@generatePdf");
    Route::get('/invoice-sendmail/{invoiceid}',"Admin\InvoiceController@sendInvoiceMail");//send mail with invoice pdf attached

    Route::get('/changepassword','Admin\DashboardController@changePassword');
    Route::post('/updatepassword','Admin\DashboardController@updatePassword')->name('password.update');

    /*
     * end rojina code
     * */



});

