// controller for street table(record)
app.controller('streetController', ['$scope', 'StreetService', function ($scope, StreetService) {
    $scope.street = {};
    $scope.message = "street";

    $scope.fetch = true;
    console.log($scope.fetch);
    StreetService.query(function (data) {
        $scope.streets = data;
        console.log($scope.streets);
    }, function (data) {

    });
    $scope.createStreet = function () {
        if ($scope.streetForm.$valid) {
            console.log($scope.street);
            StreetService.save($scope.street, function (data) {
                alert("Street Added");
                $('#myStreet').modal('hide');
                console.log(data);
                $scope.streets.push(data);
                console.log($scope.streets);
                $scope.street = {};
            }, function (data) {
                $scope.flash = {'has': true, 'message': "Couldn't add streets"};
            });
        } else {
            console.log('The form has some errors');
            console.log($scope.streetForm.$error);

        }

    };

    $scope.updateStreet = function (id) {
        console.log("The orginal id:" + id);
        StreetService.update({id: id}, $scope.street, function (data) {
            var oldItem = $scope.streets.filter(function (e) {
                return e.id == id;
            });
//                $scope.flash = {'has': true, 'message': "Street Updated Successfully"};
            alert("Street Updated");
            $('#myStreet').modal('hide');
            var orginalIndex = $scope.streets.indexOf(oldItem[0]);
            console.log(orginalIndex);
            $scope.streets.splice(orginalIndex, 1);
            $scope.streets.splice(orginalIndex, 0, data);
            $scope.street = {};
        }, function () {

        });
    };
    $scope.editStreet = function (id) {
        console.log('Editing Street' + id);
        $scope.flash = {};
        StreetService.get({id: id}, function (data) {
            console.log(data);
            $scope.street = data;
        }, function (data) {

        });

    };
    $scope.deleteStreet = function (id) {
        console.log('Deleting street' + id);
        if (confirm('Sure to Delete?')) {
            StreetService.delete({id: id}, function (data) {
                console.log(data);
                alert('deleted');
                $scope.streets = $scope.streets.filter(function (e) {
                    return  e.id != id;
                });
            }, function (data) {

            });
        }
    };
    $scope.openCreateForm = function () {
        console.log('opening Form');
        $scope.street = {};
        $scope.flash = {};
        $scope.modalData = {'modalHeader': 'Add Street name',
            'create': true,
            'edit': false};

    };
    $scope.openEditForm = function () {
        console.log('opening Edit Form');
        $scope.flash = {};
        $scope.modalData = {'modalHeader': 'Edit Street',
            'create': false,
            'edit': true};

    };
    $scope.isSelected = function (item) {
        return $scope.street.status === item;
    }

}]);

//contollet for item table(record)
app.controller('itemController', ['$scope', 'ItemService', function($scope, ItemService){
    $scope.item = {};
    $scope.message = "item";
    $scope.fetch = true;
    console.log($scope.fetch);
    ItemService.query(function(data){
        $scope.items=data;
        console.log($scope.items);
    },function(data){

    });

    //add recored to the table
    $scope.createItem = function () {
        if ($scope.itemForm.$valid) {
            console.log($scope.item);
            ItemService.save($scope.item, function (data) {
                alert("Item Added");
                $('#myItem').modal('hide');
                console.log(data);
                $scope.items.push(data);
                console.log($scope.items);
                $scope.item = {};
            }, function (data) {
                $scope.flash = {'has': true, 'message': "Couldn't add Items"};
            });
        } else {
            console.log('The form has some errors');
            console.log($scope.streetForm.$error);

        }

    };
    $scope.deleteItem = function (id) {
        console.log('Deleting item' + id);
        if (confirm('Sure to Delete?')) {
            ItemService.delete({id: id}, function (data) {
                console.log(data);
                alert('deleted');
                $scope.items = $scope.items.filter(function (e) {
                    return  e.id != id;
                });
            }, function (data) {

            });
        }
    };
    $scope.openCreateForm = function(){
        console.log('opening form');
        $scope.item = {};
        $scope.flash = {};
        $scope.modalData = {'modalHeader': 'Add Item Name',
                            'create': true,
                            'edit': false};
    }
}]);

//contollet for sample table(record)
app.controller('sampleController', ['$scope', 'SampleService', function($scope, SampleService){
    $scope.sample = {};
    $scope.message = "sample";
    $scope.fetch = true;
    console.log($scope.fetch);
    SampleService.query(function(data){
        $scope.samples=data;
        console.log($scope.samples);
    },function(data){

    });


    $scope.deleteSample = function (id) {
        console.log('Deleting Sample' + id);
        if (confirm('Sure to Delete?')) {
            SampleService.delete({id: id}, function (data) {
                console.log(data);
                alert('deleted');
                $scope.samples = $scope.samples.filter(function (e) {
                    return  e.id != id;
                });
            }, function (data) {

            });
        }
    };
}]);

//contollet for record table(record)
app.controller('recordController', ['$scope', '$http', 'configObject', 'StreetService', 'ItemService', 'RecordService', function($scope, $http, configObject, StreetService, ItemService, RecordService){
    var url = location.href;
    var index = url.lastIndexOf('/');
    var street_id = url.substr(index+1);
    // console.log(street_id);

    $scope.initFunction=function(){
        $http.post(configObject.getEmployeeUrl() + 'getRecords',{'street_id': street_id}).then(function (data) {
            // console.log(data);
            $scope.items = data.data.items;
            // console.log('items');
            // console.log($scope.items);
            $scope.itemCount=data.data.countRecord;
            $scope.streetName=data.data.streetName.street_name;
            // console.log($scope.streetName);
            // console.log('itemcount');
            // console.log($scope.itemCount);
        });
    };

    $scope.getStreetName=function(){
        return $scope.streetName;
    }

    $scope.getItemCountByStreet=function(itemId,entry_type){
        // console.log('parameters');
        // console.log(itemId);
        // console.log(entry_type);

        var ic = 0;
        $scope.itemCount.forEach(function(el){
            if(entry_type === el.entry_type) {
                if (itemId == el.item_id) {
                    ic = el.item_count;
                }
            }
        });
        // console.log(ic);
        return ic;
    }
    //open form
    $scope.openAddRecordForm=function(itemName, itemId,e_type){
        // console.log('item id');
        // console.log(itemId);
        // console.log(itemName);
        // console.log(e_type);
        $scope.veh={
                            'street_id': street_id,
                            'item_id': itemId
                          };

        $scope.modalData = {'modalHeader': 'Vehicle Detail',
            'item_name': itemName,
            'item_id': itemId,
            'e_type': e_type
            };
        // console.log($scope.modalData);
    }

    //add vehicle detail with manifest image
    $scope.addVehicleDescription1=function(){
        $scope.isDisabled = true;
        console.log('Manifest');
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", $scope.files[0]);
        $http.post(configObject.getEmployeeUrl() + 'uploadfile', fd, {
            withCredentials: true,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        }).then(function (data) {
            $scope.veh.image = data.data;
            console.log($scope.veh);
            $http.post(configObject.getEmployeeUrl() + 'addvehdetail', $scope.veh).then(function (data) {
                // console.log(data);
                $('#myRecord').modal('hide');
                $scope.isDisabled = false;
                $scope.veh={};
                // $scope.initFunction();
            });

        });
    }

    //read uploaded imaae file
    $scope.uploadedFile = function(element) {
        var reader = new FileReader();
        reader.onload = function(event) {
            $scope.$apply(function($scope) {
                $scope.files = element.files;
                $scope.src = event.target.result
            });
        }
        reader.readAsDataURL(element.files[0]);
    }


    // add vehich detail without manifest
    $scope.addVehicleDescription=function(){
        console.log('Unauthorized');
        $scope.isDisabled = true;
        $http.post(configObject.getEmployeeUrl() + 'addvehdetail', $scope.veh).then(function (data) {
            // console.log(data);
            $('#myRecord').modal('hide');
            $scope.isDisabled = false;
            $scope.veh={};
            // $scope.initFunction();
        });

    }

    //add item count record for vehicle
    $scope.addRecord=function(pay_method, itemId){
        console.log(pay_method);
        console.log(itemId);
        $scope.addrecord={'street_id': street_id,
                          'item_id': itemId,
                          'pay_method': pay_method};
        console.log($scope.addrecord);
        RecordService.save($scope.addrecord, function(data){
            // if(pay_method!=='Unauthorized Entry' && pay_method!=='Manifest'){
                $scope.initFunction();
            // }else{
                console.log('other detail');
            // }
        },function(data){

        });
    }

    //add item count record for pedestrian
    $scope.addRecord2=function(pay_method, itemId){
        // console.log(pay_method);
        // console.log(itemId);
        $scope.addrecord={'street_id': street_id,
            'item_id': itemId,
            'pay_method': pay_method};
        // console.log($scope.addrecord);
        RecordService.save($scope.addrecord, function(data){
            $scope.initFunction();
        },function(data){

        });
    }
    $scope.initFunction();
}]);
