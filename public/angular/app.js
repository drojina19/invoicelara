var app = angular.module("app", ["ngRoute", "ngResource"]);
app.factory("configObject", function () {
    return {
      baseUrl: 'http://localhost:8000/',
      prefix:'admin/',
      getFullBaseUrl:function(){
        return this.baseUrl+this.prefix;
      },
      getEmployeeUrl:function(){
          return this.baseUrl+'employee/';
      }
    }
});
app.config(["$httpProvider",function($httpProvider){
        //$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
}]);

//service module for user table(record)
app.service("UserService",["$resource","configObject",function($resource,configObject){
    var user=$resource(configObject.getFullBaseUrl()+'users/:id',{id:'@id'},{'query':{'method':'GET','isArray':true,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    console.log(configObject.getFullBaseUrl());
    return user;
}]);


//service module for role table(record)
app.service("RoleService",["$resource","configObject",function($resource,configObject){
    var role=$resource(configObject.getFullBaseUrl()+'roles/:id',{id:'@id'},{'query':{'method':'GET','isArray':false,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    console.log(configObject.getFullBaseUrl());
    return role;
}]);


//service module for permission table(record)
app.service("PermissionService",["$resource","configObject",function($resource,configObject){
    var permission=$resource(configObject.getFullBaseUrl()+'permissions/:id',{id:'@id'},{'query':{'method':'GET','isArray':false,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    console.log(configObject.getFullBaseUrl());
    return permission;
}]);

//service module for street table(record)
app.service("StreetService",["$resource","configObject",function($resource,configObject){
  var street=$resource(configObject.getFullBaseUrl()+'streets/:id',{id:'@id'},{'query':{'method':'GET','isArray':true,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
  // console.log(configObject.getFullBaseUrl());
  return street;
}]);


//service module for item:(vehicle & pedestrian data) table(item)
app.service("ItemService",["$resource","configObject",function($resource,configObject){
    var item=$resource(configObject.getFullBaseUrl()+'items/:id',{id:'@id'},{'query':{'method':'GET','isArray':true,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    // console.log(configObject.getFullBaseUrl());
    return item;
}]);

//service module for sample table(sample)
app.service("SampleService",["$resource","configObject",function($resource,configObject){
    var sample=$resource(configObject.getFullBaseUrl()+'sample/:id',{id:'@id'},{'query':{'method':'GET','isArray':true,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    // console.log(configObject.getFullBaseUrl());
    return sample;
}]);


//service module for records table(record)
app.service("RecordService",["$resource","configObject",function($resource,configObject){
    var record=$resource(configObject.getEmployeeUrl()+'records/:id',{id:'@id'},{'query':{'method':'GET','isArray':false,'headers':{'X-Requested-With':'XMLHttpRequest'}}, 'update':{method:'PATCH','headers':{'X-Requested-With':'XMLHttpRequest'}}});
    // console.log(configObject.getFullBaseUrl());
    return record;
}]);
