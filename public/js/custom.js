
        $('#datatable-invoice').dataTable({
            "order": [[3,"DESC"]]
        });

        var table =  $('#datatable-invoice').DataTable();
        console.log(table);
        $('#dropdown1').on('change', function () {
//            var c =$(this).val();
            table.columns(7).search(this.value).draw();
        } );
        $('#sel-cust').on('change', function () {
//            var c =$(this).val();
            table.columns(2).search(this.value).draw();
        } );
        $('#sel-supplier').on('change', function () {
//            var c =$(this).val();
            table.columns(1).search(this.value).draw();
        } );


/************************************************************************************************************/
/************************************************************************************************************/
//product start
$("#adding_newrow").click(function () {
    var count=($("tbody tr").length);
    var a = $('.row-product').clone();
    a.removeClass("row-product");
    a.removeClass("hidden");
    a.find('input').attr('disabled',false);
    a.find('td.increase').html(count);
    // alert(a);
    $("tbody").append(a);

    // alert('ads');
});


function removeRow(el) {
    $(el).parents("#rp").remove();
    arrange();
    return false;

}

function arrange() {
    var i = 0;
    $("tbody tr").each(function() {
        $(this).find("td.increase").html(i);
        i++;
    });
}

$('#issue_date').daterangepicker({
    locale: {
        format: 'DD-MM-YYYY'
    },
    singleDatePicker: true,
    singleClasses: "picker_3"
}, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
});

$('#due_date').daterangepicker({
    locale: {
        format: 'DD-MM-YYYY'
    },
    singleDatePicker: true,
    singleClasses: "picker_3"
}, function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
});
//product end

/************************************************************************************************************/
/************************************************************************************************************/

/*  Starts Here For Invoice Row Addition Dynamically */

$("#adding_row").click(function () {
    var count=($("tbody tr").length);
    var b = $("#product").clone();
    b.removeClass("product-hidden");
    b.removeClass("hidden");
    b.find('select, textarea').attr('disabled',false);
    b.find('td.increase').addClass("inc1");
    b.find('td.inc1').html(count);

    $("#inv-detail").append(b);

    
});


function removeRow(el) {
    $(el).parent().parent().remove();
    arrange();
    subtotal();
    var g_type=$("#gst_type").val();
    if(g_type==="GTS10")
    {
        gst() ;
    }

    else if(g_type==="IncludingGST")
    {
        gstinclude();
    }
    return false;

}

function arrange() {
    var i = 1;
    $("#inv-detail tr").each(function() {
        if($(this).find("td.inc1").length!=0){
            $(this).find("td.inc1").html(i);
            i++;
        };
    });
}
/* Ends Here*/

/*To Enable the input type*/

$('#inv-detail').delegate('.product_name', 'change',function () {
    var tr=$(this).parent().parent().parent().parent();
    var _product_id=tr.find('.product_name').val();
    var product_id=parseInt(_product_id);
    tr.find(".quantity, .rate, .amount1").attr('disabled',false);
});

/*Ends Here*/

/* To Calculate Total*/

$("#inv-detail").delegate(".quantity, .rate","keyup",function () {
    var tr=$(this).parent().parent();
    var qty=tr.find('.quantity').val();
    quantity=parseFloat(qty);
    var rat=tr.find('.rate').val()-0;
    rate=parseFloat(rat);
    var amount= quantity * rate;
    console.log(amount);
    tr.find(".amount, .amount1").val(parseFloat(amount).toFixed(2));
    subtotal();
    var g_type=$("#gst_type").val();
    if(g_type==="GTS10")
    {
        gst() ;
    }

    else if(g_type==="IncludingGST")
    {
        gstinclude();
    }

});
/* For calculating subtotal amount*/
function subtotal() {

    var t=0;
    $('.amount').each(function (i,element) {
        var amt=$(this).val()-0;
        t+=amt;
    });
    $(".subtotal,.subtotal1").val(parseFloat(t).toFixed(2));

}
/*sub total() method ends*/

/*For calculating gst -10% value*/
function gst() {
    var t=0;
    var gstvalue=0;
    $('.amount').each(function (i,element){
        var amt=$(this).val()-0;
        t+=amt;
    });
    gstvalue=t *.1;

    $(".gst,.gst1").val(parseFloat(gstvalue).toFixed(2));
    grandtotal();
}

function gstinclude(){
    var total=$('.subtotal').val()-0;
    var gstvalue=(total/1.1)*0.1;

    $(".gst,.gst1").val(parseFloat(gstvalue).toFixed(2));
    $(".grandtotal,.grandtotal1").val(parseFloat(total).toFixed(2));
}
/*gst() method ends here*/

/*For calculating gst included value*/
function gstChanged(val){
    if(val==="GTS10")
    {
        gst() ;
    }

   else if(val==="IncludingGST")
    {
        gstinclude();
    }

}
/*ends here*/
/* For Calculating Grand Total*/
 function grandtotal() {
     var grandtotal=0;
     var total=$('.subtotal').val()-0;
     var gst=$('.gst').val()-0;
     grandtotal=total + gst;
     $(".grandtotal,.grandtotal1").val(parseFloat(grandtotal).toFixed(2));
 }
//invoice end
/************************************************************************************************************/
/************************************************************************************************************/


/*For calculating new amount paid*/

$('#amt_paid ').on('keyup change',function()
{
    var paid=$(this).val();
    var totalamt_owed= parseFloat($('#amt_owed').val().replace("$","")).toFixed(2);
    var left_due =totalamt_owed -paid;
    if(left_due<0)
    {
        alert("You have enter excess amount");
        $('#amt_paid').val("");
        $('#newamt_owed').val(totalamt_owed);
    }
    else
    {
        $('#newamt_owed').val(parseFloat(left_due).toFixed(2));
    }
});

/*
* print invoice
* */

$(".printInvoice").click(function(){
    //alert($(this).attr('data-url'));
    var printaddr=$(this).attr('data-url');
//                    console.log(printaddr);

    window.open(printaddr,"","width=600,height=600").print();
});

