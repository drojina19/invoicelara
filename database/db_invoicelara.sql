-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2017 at 09:39 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_invoicelara`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `attn`, `abn`, `email`, `phone1`, `phone2`, `street_address`, `suburb`, `state`, `zipcode`, `country`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(22, 'SHRAMIKA ENTERPRISES KUTANI PIS. UDHYOG.', 'zczxc', '1234', 'yagya_adhikari@hotmail.com', '123', '123', 'jjasd', 'ghagdhj', 'NSW', '1234', 'Australia', 1, NULL, NULL, NULL),
(23, 'Himalayan Grocer Suppliers ', 'Kamal Sharma', '45608659169', 'order@himalayangrocer.com.au', '0423653023', '0423653023', '17-19 Henley Road ', 'Homebush West', 'NSW', '2140', 'Australia', 1, NULL, NULL, NULL),
(24, 'Aman Group Services', 'Kamal Ismayel', '93942088633', 'amangservices@gmail.com', '0450073626', '0450073626', 'Unit 7 36-40 LOUIS STREET', 'GRANVILLE', 'NSW', '2142', 'Australia', 1, NULL, NULL, NULL),
(25, 'SITOULA & ASSOCIATES ', 'SITOULA & ASSOCIATES ', ' 39168814999', 'info@sitoula.com.au', '(02) 9188 1575', '', '1/126 Marsden St ', 'Paramatta NSW', 'NSW', '2150', 'Australia', 1, NULL, NULL, NULL),
(27, 'ABP(AkShyaralaya Bhasha Pratisthan)', 'Gaunle Baldev Adhikari', '111111111', 'gaunlebaldev@gmail.com', '+9779841398281', '', 'Naikap', 'Kathmandu', 'NSW', '+977', 'Nepal', 1, NULL, NULL, NULL),
(28, 'GBP ( Gaunle Baldev Pratisthan)', 'Gaunle Baldev Adhikari', '22222222', 'gaunlebaldev@gmail.com', '+9779841398281', '', 'Neelakantha Municipality', 'Dhading', 'NSW', '+977', 'Nepal', 1, NULL, NULL, NULL),
(35, 'Subha Services Pty Ltd', 'Mr.Bharat Subedi', '69613035167', 'admin@subhaservices.com', '0416476168', '', '4b/75 Lakemba Street', 'Belmore', 'NSW', '2192', 'Australia', 1, NULL, NULL, NULL),
(39, '909 IT Solutions Pty Ltd', 'Dan Dhakal', '92601718889', 'info@909it.com.au', '466608696', '', '18 Burford Street', 'Minto', 'NSW', '2566', 'Australia', 1, NULL, NULL, NULL),
(41, 'Oceania Education Solutions ', 'Sandip Khanal / Sharad Bhusal', '93604602360', 'admin@oceaniaeducation.com.au', '0452575800', '0430911912', 'Suite 308, Level 3 Fayworth House 379-383 Pitt Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(44, 'Namaste Migration & Education', 'Surendra Sigdel', '19020059068', 'namaste@naafa.com.au', '0290902257 ', '0423273302', 'Suite 1406, level 14/ 447 Kent Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(45, 'University of New South Wales', 'Satish Paudyal,Infrastructure Designer', '', 'spaudyal@unsw.edu.au', '0293853160', '0439108847', 'Level 13,Library Building,Stage 2', 'Randwick', 'NSW', '2052', 'Australia', 1, NULL, NULL, NULL),
(46, 'Best Choice Travels & Tours', 'Rudra Aryal', '57612688525', 'info@bestchoicetravels.com.au', '0401349816', '0431601375', '5 61-63 Gray St ', 'Kogarah ', 'NSW', '2217', 'Australia', 1, NULL, NULL, NULL),
(47, 'BNB Consulting Pty Ltd', 'Bhisma Bhandari', '61607017274', 'bhismabhandari@gmail.com', '0432277072', '', '15 A Worsley Street ', 'East Hills', 'NSW', '2213', 'Australia', 1, NULL, NULL, NULL),
(48, 'Smart Education and Visa Services', 'Sushil Nepal', '24167275425', 'info@smarteducationvisa.com.au', '0423954894', '0289705215', 'Suite211,Level 2,250 Pitt Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(51, 'Deepesh Garbuja', 'Deepesh', '', 'garbuja7@hotmail.com', '0405234284', '', '19/3-13 Comer Street', 'Burwood', 'NSW', '2134', 'Australia', 1, NULL, NULL, NULL),
(52, 'Sawan Ranjitkar', 'Sawan', '', 'sawanranjit@gmail.com', '0424720005', '', '3/11-13 Hudson Street', 'Hurstville', 'NSW', '2220', 'Australia', 1, NULL, NULL, NULL),
(53, 'Sabin Thapaliya', 'Sabin', '', 'sabinthapaliya@yahoo.com', '0468755418', '', '2/16 Subway Road', 'Rockdale', 'NSW', '2216', 'Australia', 1, NULL, NULL, NULL),
(54, 'Bipin Thapa', 'Bipin', '', 'bipin.thapa01@gmail.com', '0412764216', '', 'Unit 6 , 3 Hall Street', 'Auburn', 'NSW', '2144', 'Australia', 1, NULL, NULL, NULL),
(55, 'Bhanu Bhakta Pokhrel', 'Bhanu', '', 'bhanupkhrl@gmail.com', '0452544268', '', '104 boundary street', 'Roseville', 'NSW', '2069', 'Australia', 1, NULL, NULL, NULL),
(56, 'Kapil Maharjan', 'Kapil', '', 'kapil@fifthbadsector.com', '0406002102', '', '8/40 Robert Street', 'Ashfield', 'NSW', '2131', 'Australia', 1, NULL, NULL, NULL),
(57, 'Ranjan Shrestha', 'Ranjan', '', 'ranjanshrestha_b@hotmail.com', '0466665937', '', 'Unit 7/ 5-7 saywell Road', 'Macquarie Fields', 'NSW', '2564', 'Australia', 1, NULL, NULL, NULL),
(58, 'Tulsi Ram Sharma', 'Tulsi', '', 'tulsi_sharma@hotmail.com', '0422918750', '', '15 Macquarie Ave,', 'Lumeah', 'NSW', '2560', 'Australia', 1, NULL, NULL, NULL),
(59, 'Pourakh Dhakal', 'Pourakh', '', 'pourakh70@gmail.com', '0403628066', '', '9 Pilgrim Ave', 'Strathfield', 'NSW', '2135', 'Australia', 1, NULL, NULL, NULL),
(60, 'Prakash K C', 'Prakash', '', 'kc.pkash@gmail.com', '0449104034', '', '11 Bridge Street', 'Penshurst', 'NSW', '2222', 'Australia', 1, NULL, NULL, NULL),
(61, 'Dipsana Kandel', 'Dipsana', '', 'dipsanakandel88@gmail.com', '0415477783', '', '7-52 Jacob Street', 'Bankstown', 'NSW', '2200', 'Australia', 1, NULL, NULL, NULL),
(62, 'Dipak Dhamala', 'Dipak', '26147310969', 'info@himalayan-experience.net', '0883634803', '0430488222', '133 Portush Road ', 'Evandale', 'SA', '5069', 'Australia', 1, NULL, NULL, NULL),
(63, 'Keeming Rajbhandari', 'Keeming', '', 'keeming1@gmail.com', '0449930414', '', 'Unit 6 15-23 Marion Street', 'Parramatta', 'NSW', '2150', 'Australia', 1, NULL, NULL, NULL),
(64, 'Epping Eatery Pty Ltd T/As Roaming Pest Control', 'Uday Lohani', '75141976972', 'u_lohani@hotmail.com', '0402813266', '0402813266', '77 Somerville Road', 'Hornsby Height', 'NSW', '2077', 'Australia', 1, NULL, NULL, NULL),
(65, 'Shreeshu Rana Chhetri', 'Kopila', '', 'shreeshu.kopila@hotmail.com', '0433442599', '', '2a banool circuit ', 'bomaderry ', 'NSW', '2541', 'Australia', 1, NULL, NULL, NULL),
(66, 'Neeta KC', 'Neeta', '', 'nitapuki@hotmail.com', '0420818116', '', '77 Somerville Road', 'Hornsby Height', 'NSW', '2077', 'Australia', 1, NULL, NULL, NULL),
(67, 'Dinesh karki', 'Dinesh', '', 'dineshkarki907@yahoo.com', '0414277990', '', '13/28 William Street ', 'Granville', 'NSW', '2142', 'Australia', 1, NULL, NULL, NULL),
(68, 'Deepa Adhikari', 'Deepa', '', 'nanu39.da@gmail.com', '0406189895', '', '14/6 Parkhome Circuit', 'Englorie Park', 'NSW', '2560', 'Australia', 1, NULL, NULL, NULL),
(69, 'Bijay Neupane', 'Bijay', '', 'nbeejeh@gmail.com', '0426299934', '', '6/11 Bridge Street', 'Penshurst', 'NSW', '2222', 'Australia', 1, NULL, NULL, NULL),
(70, 'Gaurav Karki', 'Gaurav', '', 'samraj_gaurab@hotmail.com', '0425437514', '', '12/11 Lyons Street', 'Strathfield', 'NSW', '2135', 'Australia', 1, NULL, NULL, NULL),
(71, 'Prabin Shahi', 'Prabin', '', 'pravin_33@hotmail.com', '0450302102', '', '53 Millett Street', 'Hurstville', 'NSW', '2220', 'Australia', 1, NULL, NULL, NULL),
(72, 'Sushil Nepal', 'Sushil', '', 'sushil@smarteducationvisa.com.au', '0423954894', '', 'Suite211, Level 2, 250Pitt Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(73, 'Tika Tulachan', 'Tika', '', 'dipsanakandel88@gmail.com', '0415477783', '', '22 Subway Road', 'Rockdale', 'NSW', '2216', 'Australia', 1, NULL, NULL, NULL),
(74, 'Kripa Adhikari Sapkota', 'Kripa', '', 'dipsanakandel88@gmail.com', '0415477783', '', 'Unit 6/206 Pacific Highway', 'Greenwich', 'NSW', '2065', 'Australia', 1, NULL, NULL, NULL),
(75, 'Dipak Ojha', 'Dipak', '', 'deeps-ojhs@yahoo.com', '0416556123', '', '6-7 Lidbury Street ', 'Berela', 'NSW', '2141', 'Australia', 1, NULL, NULL, NULL),
(76, 'Rupak Ojha', 'Rupak', '', 'rupakojha1@gmail.com', '0450969818', '', '6-7 Lidbury Street, ', 'Berela', 'NSW', '2200', 'Australia', 1, NULL, NULL, NULL),
(77, 'Srijana Subedi', 'Srijana', '', 'dipsanakandel88@gmail.com', '0450316485', '', 'Englorie park', 'parkholme circuit', 'NSW', '2560', 'Australia', 1, NULL, NULL, NULL),
(78, 'dipsana kandel', 'dip', '', 'dipsanakandel88@gamil.com', '0415477783', '', '52/7 jacobs street', 'Bankstown', 'NSW', '2200', 'Australia', 1, NULL, NULL, NULL),
(79, 'Shikhi Japanese restaurant', 'Koji', '', 'shikiaustralia@optusnet.com.au', '0292522431', '', '35-75 Harrington Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(80, 'Righton Finance', 'Deepak Aryal', '81592054326', 'deepsaryal@yahoo.com', '0410027440', '', '25A Douglas Road', 'Quakershill', 'NSW', '2763', 'Australia', 1, NULL, NULL, NULL),
(81, 'Link People Pty Ltd', 'Rod Morris', '13608721768', 'accounts@linkpeople.com.au', '0298968934 ', '0401031773 ', 'Unit 10,2-4 Picrite Close', 'Pemulwuy', 'NSW', '2145', 'Australia', 1, NULL, NULL, NULL),
(82, 'IMAX Property Management Pty Ltd', 'Bipin Thapa', '74613912495', 'bipin.thapa01@gmail.com', '0412764216', '', 'Unit 6, 1-3 Hall Street', 'Auburn', 'NSW', '2144', 'Australia', 1, NULL, NULL, NULL),
(83, 'A1 Group Services Pty Ltd', 'Pradip Dahal', '66617773314', 'pradip@a1groupservices.com.au', '0421729605', '0421729605', '46a Duke Street', 'Campsie', 'NSW', '2194', 'Australia', 1, NULL, NULL, NULL),
(84, 'AGFS GROUP (AUSTRALIA) PTY LTD', 'Mac Kc', '43617914593', 'admin@agfsconnect.com.au', '0285423501', '0285423501', 'Suite 8, 3A Railway Pde', 'Kogarah', 'NSW', '2217', 'Australia', 1, NULL, NULL, NULL),
(85, 'D&E Property Development Agents', 'Dipenfra Chhetri', '61617109949', 'accounts@dneinvestment.com.au', '0280579296', '0449247441', '57 Kings Road', 'Brighton - Le _ Sands', 'NSW', '2216', 'Australia', 1, NULL, NULL, NULL),
(86, 'Dhakal Investment Pty Ltd', 'Sudip Dhakal', '53613855280', 'accounts@dhakalinvestment.com.au', '0421460335', '', '9 Bryant Street', 'Rockdale', 'NSW', '2216', 'Australia', 1, NULL, NULL, NULL),
(87, 'Shanti Dhakal', 'Yagya', '', 'yagya_adhikari@hotmail.com', '0404875800', '', '1/30 Kerr Pde', 'Auburn', 'NSW', '2144', 'Australia', 1, NULL, NULL, NULL),
(88, 'Three Streets Driving School', 'Swatantra', '83828802971', 'swatantra9@hotmail.com', '0421942764', '', 'Unit 2/ 30 Pritchard Street', 'Wentworthville', 'NSW', '2145', 'Australia', 1, NULL, NULL, NULL),
(89, 'Yagya Adhikari', 'Yagya', '', 'yagya_adhikari@hotmail.com', '0421673915', '', '1 Hall Street', 'Auburn', 'NSW', '2144', 'Australia', 1, NULL, NULL, NULL),
(90, 'Roaming Pest Control', 'Uday Lohani', '75141976972', 'u_lohani@hotmail.com', '0402813266', '0402813266', '77 Somerville Road', 'Hornsby Height', 'NSW', '2077', 'Australia', 1, NULL, NULL, NULL),
(91, 'Mac Kc', 'Mac', '', 'test@geniusitsolutions.com.au', '0425698548', '0280569874', '5b 186 Canterbury Road', 'Canterbury', 'NSW', '2193', 'Australia', 1, NULL, NULL, NULL),
(92, 'Pratik Devkota', 'Pratik', '', 'pat.devkota@gmail.com', '0424715177', '', '1/28 Parnell Street', 'Strathfield', 'NSW', '2135', 'Australia', 1, NULL, NULL, NULL),
(93, 'Safenet Security', 'Rachana Sachdev', '76165487669', 'rachna_pahwa2007@yahoo.com', '0432501243', '', '2 Grovedale Cl', 'Thomastown', 'VIC', '3074', 'Australia', 1, NULL, NULL, NULL),
(94, 'R2M Accounting & Taxation Services', 'Mac Kc', '91601702167', 'r2mbusinessgroup@gmail.com', '85422217', '0431780817', 'Suite 8,3A Railway Parade', 'Kogarah', 'NSW', '2217', 'Australia', 1, NULL, NULL, NULL),
(95, 'Peepal Bot Pty Ltd', 'Milan Khanal', '619912720', 'khanal_0123@yahoo.com', '0404830547', '', '8/32 Park Avenue', 'Westmead', 'NSW', '2145', 'Australia', 1, NULL, NULL, NULL),
(96, 'Team Consult', 'Buddhi Dallakoti', '45609546492', 'teamconsult.au@gmail.com', '0292680217', '0412622047', 'Suit7,Level5 Pitt Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL, NULL),
(97, 'Gopali Business Services T/AS Sharp Eyes Group Solutions', 'Gagan Pahwa', '30612311225', 'gagan@sharpeyesgroupsolutions.com.au', '0261345909', '', 'Suit 7/575 Woodville Road', 'Guildford', 'NSW', '2161', 'Australia', 1, NULL, NULL, NULL),
(98, 'Superb Cleaning Newcastle', 'Rabin Shrestha', '42212693642', 'ravin_shretha2000@yahoo.com', '0451623422', '', '23 discovery drive', 'Fletcher', 'NSW', '2287', 'Australia', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `issue_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gst` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gst_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grandtotal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amt_owed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_mail_sent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_no`, `supplier_id`, `customer_id`, `payment_id`, `issue_date`, `due_date`, `total`, `gst`, `gst_type`, `grandtotal`, `amt_owed`, `status`, `created_at`, `updated_at`, `last_mail_sent`, `deleted_at`) VALUES
(2, 'GEN-INV02', 1, 41, 1, '2017-10-05 06:09:46', '2016-11-09 18:15:00', '2000.00', '181.82', 'IncludingGST', '2000.00', '0.00', 1, NULL, NULL, NULL, NULL),
(3, 'GEN-INV03', 1, 45, 1, '2017-10-05 06:08:41', '2016-12-03 18:15:00', '800.00', '80.00', 'GTS10', '880.00', '880.00', 0, NULL, NULL, NULL, NULL),
(4, 'GEN-INV04', 1, 35, 1, '2017-10-05 06:08:41', '2016-11-10 18:15:00', '1700.00', '170.00', 'GTS10', '1870.00', '0.00', 1, NULL, NULL, NULL, NULL),
(5, 'GEN-INV05', 1, 46, 1, '2017-10-05 06:08:41', '2016-11-17 18:15:00', '5600.00', '560.00', 'GTS10', '6160.00', '35.00', 0, NULL, NULL, NULL, NULL),
(6, 'GEN-INV06', 1, 47, 1, '2017-10-05 06:08:41', '2016-11-21 18:15:00', '4550.00', '455.00', 'GTS10', '5005.00', '5.00', 0, NULL, NULL, NULL, NULL),
(7, 'GEN-INV07', 1, 44, 1, '2017-10-05 06:08:41', '2016-11-14 18:15:00', '1155.00', '115.50', 'GTS10', '1270.50', '0.50', 0, NULL, NULL, NULL, NULL),
(8, 'GEN-INV08', 1, 48, 1, '2017-10-05 06:08:41', '2016-11-16 18:15:00', '900.00', '90.00', 'GTS10', '990.00', '0.00', 1, NULL, NULL, NULL, NULL),
(9, 'GEN-INV09', 1, 51, 1, '2017-10-05 06:14:04', '2016-11-15 18:15:00', '480.00', '48.00', 'GTS10', '528.00', '528.00', 0, NULL, NULL, NULL, NULL),
(12, 'GEN-INV12', 1, 35, 1, '2017-10-05 06:08:41', '2016-11-24 18:15:00', '2000.00', '200.00', 'GTS10', '2200.00', '2200.00', 0, NULL, NULL, NULL, NULL),
(13, 'GEN-INV13', 1, 52, 1, '2017-10-05 06:08:41', '2016-11-29 18:15:00', '1000.00', '100.00', 'GTS10', '1100.00', '0.00', 1, NULL, NULL, NULL, NULL),
(14, 'GEN-INV14', 1, 53, 1, '2017-10-05 06:08:41', '2016-11-29 18:15:00', '1728.00', '172.80', 'GTS10', '1900.80', '1900.80', 0, NULL, NULL, NULL, NULL),
(15, 'GEN-INV15', 1, 54, 1, '2017-10-05 06:09:46', '2016-11-30 18:15:00', '1500.00', '136.36', 'IncludingGST', '1500.00', '0.00', 1, NULL, NULL, NULL, NULL),
(16, 'GEN-INV16', 1, 39, 1, '2017-10-05 06:08:41', '2016-12-05 18:15:00', '4000.00', '400.00', 'GTS10', '4400.00', '4400.00', 0, NULL, NULL, NULL, NULL),
(17, 'GEN-INV17', 1, 55, 1, '2017-10-05 06:08:41', '2016-12-06 18:15:00', '870.00', '87.00', 'GTS10', '957.00', '957.00', 0, NULL, NULL, NULL, NULL),
(18, 'GEN-INV18', 1, 39, 1, '2017-10-05 06:08:41', '2016-12-12 18:15:00', '5800.00', '580.00', 'GTS10', '6380.00', '6380.00', 0, NULL, NULL, NULL, NULL),
(19, 'GEN-INV19', 1, 55, 1, '2017-10-05 06:08:41', '2016-12-14 18:15:00', '3650.00', '365.00', 'GTS10', '4015.00', '4015.00', 0, NULL, NULL, NULL, NULL),
(20, 'GEN-INV20', 1, 56, 1, '2017-10-05 06:08:41', '2016-12-30 18:15:00', '2272.73', '227.27', 'GTS10', '2500.00', '2500.00', 0, NULL, NULL, NULL, NULL),
(21, 'GEN-INV21', 1, 57, 1, '2017-10-05 06:08:41', '2016-12-30 18:15:00', '1365.00', '136.50', 'GTS10', '1501.50', '1501.50', 0, NULL, NULL, NULL, NULL),
(22, 'GEN-INV22', 1, 58, 1, '2016-12-10 18:15:00', '2016-12-17 18:15:00', '910.00', '91.00', '', '1001.00', '1001.00', 0, NULL, NULL, NULL, NULL),
(23, 'GEN-INV23', 1, 59, 1, '2017-10-05 06:09:46', '2016-12-19 18:15:00', '500.00', '45.45', 'IncludingGST', '500.00', '500.00', 0, NULL, NULL, NULL, NULL),
(24, 'GEN-INV24', 1, 39, 1, '2017-10-05 06:08:41', '2016-12-27 18:15:00', '6800.00', '680.00', 'GTS10', '7480.00', '7480.00', 0, NULL, NULL, NULL, NULL),
(25, 'GEN-INV25', 1, 60, 1, '2017-10-05 06:08:41', '2017-01-08 18:15:00', '455.00', '45.50', 'GTS10', '500.50', '500.50', 0, NULL, NULL, NULL, NULL),
(26, 'GEN-INV26', 1, 39, 1, '2017-10-05 06:08:41', '2017-01-23 18:15:00', '4080.00', '408.00', 'GTS10', '4488.00', '4488.00', 0, NULL, NULL, NULL, NULL),
(27, 'GEN-INV27', 1, 61, 1, '2017-10-05 06:08:41', '2017-01-23 18:15:00', '1000.00', '100.00', 'GTS10', '1100.00', '1100.00', 0, NULL, NULL, NULL, NULL),
(28, 'GEN-INV28', 1, 39, 1, '2017-10-05 06:08:41', '2017-01-30 18:15:00', '9000.00', '900.00', 'GTS10', '9900.00', '9900.00', 0, NULL, NULL, NULL, NULL),
(29, 'GEN-INV29', 1, 62, 1, '2017-10-05 06:08:41', '2017-01-24 18:15:00', '29.00', '2.90', 'GTS10', '31.90', '31.90', 0, NULL, NULL, NULL, NULL),
(30, 'GEN-INV30', 1, 64, 1, '2017-10-05 06:08:41', '2017-02-02 18:15:00', '3640.00', '364.00', 'GTS10', '4004.00', '4004.00', 0, NULL, NULL, NULL, NULL),
(31, 'GEN-INV31', 2, 65, 2, '2017-10-05 06:08:41', '2017-01-29 18:15:00', '500.00', '50.00', 'GTS10', '550.00', '550.00', 0, NULL, NULL, NULL, NULL),
(32, 'GEN-INV32', 2, 66, 2, '2017-10-05 06:08:41', '2017-02-14 18:15:00', '500.00', '50.00', 'GTS10', '550.00', '550.00', 0, NULL, NULL, NULL, NULL),
(33, 'GEN-INV33', 1, 54, 1, '2017-10-05 06:08:41', '2017-02-07 18:15:00', '1360.00', '136.00', 'GTS10', '1496.00', '1496.00', 0, NULL, NULL, NULL, NULL),
(34, 'GEN-INV34', 1, 67, 1, '2017-10-05 06:08:41', '2017-02-07 18:15:00', '1000.00', '100.00', 'GTS10', '1100.00', '1100.00', 0, NULL, NULL, NULL, NULL),
(35, 'GEN-INV35', 1, 68, 1, '2017-10-05 06:09:46', '2017-02-14 18:15:00', '1000.00', '90.91', 'IncludingGST', '1000.00', '1000.00', 0, NULL, NULL, NULL, NULL),
(36, 'GEN-INV36', 1, 69, 1, '2017-10-05 06:09:46', '2017-02-07 18:15:00', '500.00', '45.45', 'IncludingGST', '500.00', '500.00', 0, NULL, NULL, NULL, NULL),
(37, 'GEN-INV37', 1, 70, 1, '2017-01-24 18:15:00', '2017-02-21 18:15:00', '910.00', '91.00', '', '1001.00', '1001.00', 0, NULL, NULL, NULL, NULL),
(38, 'GEN-INV38', 1, 45, 1, '2017-10-05 06:08:41', '2017-02-28 18:15:00', '800.00', '80.00', 'GTS10', '880.00', '880.00', 0, NULL, NULL, NULL, NULL),
(39, 'GEN-INV39', 1, 71, 1, '2017-10-05 06:08:41', '2017-02-10 18:15:00', '455.00', '45.50', 'GTS10', '500.50', '500.50', 0, NULL, NULL, NULL, NULL),
(40, 'GEN-INV40', 1, 39, 1, '2017-10-05 06:08:41', '2017-02-24 18:15:00', '9000.00', '900.00', 'GTS10', '9900.00', '9900.00', 0, NULL, NULL, NULL, NULL),
(41, 'GEN-INV41', 1, 72, 1, '2017-10-05 06:09:46', '2017-02-09 18:15:00', '250.00', '22.73', 'IncludingGST', '250.00', '250.00', 0, NULL, NULL, NULL, NULL),
(42, 'GEN-INV42', 2, 60, 2, '2017-10-05 06:09:46', '2017-02-16 18:15:00', '500.00', '45.45', 'IncludingGST', '500.00', '500.00', 0, NULL, NULL, NULL, NULL),
(43, 'GEN-INV43', 1, 92, 2, '2017-10-05 06:09:46', '2017-02-15 18:15:00', '550.00', '50.00', 'IncludingGST', '550.00', '550.00', 0, NULL, NULL, NULL, NULL),
(44, 'GEN-INV44', 1, 92, 1, '2017-10-05 06:09:46', '2017-02-15 18:15:00', '250.00', '22.73', 'IncludingGST', '250.00', '250.00', 0, NULL, NULL, NULL, NULL),
(45, 'GEN-INV45', 1, 39, 2, '2017-10-05 06:08:41', '2017-02-21 18:15:00', '3200.00', '320.00', 'GTS10', '3520.00', '3520.00', 0, NULL, NULL, NULL, NULL),
(46, 'GEN-INV46', 1, 75, 1, '2017-10-05 06:08:41', '2017-02-24 18:15:00', '900.00', '90.00', 'GTS10', '990.00', '990.00', 0, NULL, NULL, NULL, NULL),
(47, 'GEN-INV47', 1, 76, 1, '2017-10-05 06:08:41', '2017-02-22 18:15:00', '1800.00', '180.00', 'GTS10', '1980.00', '1980.00', 0, NULL, NULL, NULL, NULL),
(48, 'GEN-INV48', 1, 77, 1, '2017-10-05 06:09:46', '2017-02-22 18:15:00', '1520.00', '138.18', 'IncludingGST', '1520.00', '1520.00', 0, NULL, NULL, NULL, NULL),
(49, 'GEN-INV49', 1, 39, 1, '2017-10-05 06:08:41', '2017-03-14 18:15:00', '10600.00', '1060.00', 'GTS10', '11660.00', '11660.00', 0, NULL, NULL, NULL, NULL),
(50, 'GEN-INV50', 1, 39, 1, '2017-10-05 06:08:41', '2017-03-28 18:15:00', '10060.00', '1006.00', 'GTS10', '11066.00', '11066.00', 0, NULL, NULL, NULL, NULL),
(51, 'GEN-INV51', 2, 78, 2, '2017-10-05 06:08:41', '2017-03-18 18:15:00', '500.00', '50.00', 'GTS10', '550.00', '550.00', 0, NULL, NULL, NULL, NULL),
(52, 'GEN-INV52', 1, 79, 1, '2017-10-05 06:08:41', '2017-03-22 18:15:00', '100.00', '10.00', 'GTS10', '110.00', '110.00', 0, NULL, NULL, NULL, NULL),
(53, 'GEN-INV53', 1, 80, 1, '2017-10-05 06:08:41', '2017-03-22 18:15:00', '120.00', '12.00', 'GTS10', '132.00', '132.00', 0, NULL, NULL, NULL, NULL),
(54, 'GEN-INV54', 1, 45, 1, '2017-10-05 06:08:41', '2017-04-26 18:15:00', '2560.00', '256.00', 'GTS10', '2816.00', '2816.00', 0, NULL, NULL, NULL, NULL),
(55, 'GEN-INV55', 1, 81, 2, '2017-10-05 06:08:41', '2017-04-11 18:15:00', '1797.00', '179.70', 'GTS10', '1976.70', '1976.70', 0, NULL, NULL, NULL, NULL),
(56, 'GEN-INV56', 1, 39, 1, '2017-10-05 06:08:41', '2017-04-12 18:15:00', '7120.00', '712.00', 'GTS10', '7832.00', '7832.00', 0, NULL, NULL, NULL, NULL),
(57, 'GEN-INV57', 1, 80, 1, '2017-10-05 06:08:41', '2017-04-09 18:15:00', '35.00', '3.50', 'GTS10', '38.50', '38.50', 0, NULL, NULL, NULL, NULL),
(58, 'GEN-INV58', 1, 82, 2, '2017-10-05 06:09:46', '2017-04-17 18:15:00', '2900.00', '263.64', 'IncludingGST', '2900.00', '2900.00', 0, NULL, NULL, NULL, NULL),
(59, 'GEN-INV59', 2, 39, 2, '2017-10-05 06:08:41', '2017-04-26 18:15:00', '7120.00', '712.00', 'GTS10', '7832.00', '7832.00', 0, NULL, NULL, NULL, NULL),
(60, 'GEN-INV60', 1, 45, 1, '2017-10-05 06:08:41', '2017-05-17 18:15:00', '280.00', '28.00', 'GTS10', '308.00', '308.00', 0, NULL, NULL, NULL, NULL),
(61, 'GEN-INV61', 1, 84, 1, '2017-10-05 06:08:41', '2017-04-25 18:15:00', '751.60', '75.16', 'GTS10', '826.76', '0.00', 1, NULL, NULL, NULL, NULL),
(62, 'GEN-INV62', 1, 83, 1, '2017-10-05 06:08:41', '2017-04-25 18:15:00', '620.00', '62.00', 'GTS10', '682.00', '682.00', 0, NULL, NULL, NULL, NULL),
(63, 'GEN-INV63', 1, 39, 1, '2017-10-05 06:08:41', '2017-05-07 18:15:00', '6280.00', '628.00', 'GTS10', '6908.00', '6908.00', 0, NULL, NULL, NULL, NULL),
(64, 'GEN-INV64', 1, 82, 2, '2017-10-05 06:08:41', '2017-05-17 18:15:00', '6000.00', '600.00', 'GTS10', '6600.00', '6600.00', 0, NULL, NULL, NULL, NULL),
(65, 'GEN-INV65', 1, 85, 1, '2017-10-05 06:09:46', '2017-05-26 18:15:00', '1400.00', '127.27', 'IncludingGST', '1400.00', '1400.00', 0, NULL, NULL, NULL, NULL),
(66, 'GEN-INV66', 1, 80, 1, '2017-10-05 06:08:41', '2017-06-01 18:15:00', '700.00', '70.00', 'GTS10', '770.00', '770.00', 0, NULL, NULL, NULL, NULL),
(67, 'GEN-INV67', 1, 86, 1, '2017-10-05 06:08:41', '2017-06-01 18:15:00', '1431.00', '143.10', 'GTS10', '1574.10', '1574.10', 0, NULL, NULL, NULL, NULL),
(68, 'GEN-INV68', 1, 39, 1, '2017-10-05 06:08:41', '2017-06-24 18:15:00', '14240.00', '1424.00', 'GTS10', '15664.00', '15664.00', 0, NULL, NULL, NULL, NULL),
(69, 'GEN-INV69', 1, 81, 1, '2017-10-05 06:08:41', '2017-06-08 18:15:00', '426.80', '42.68', 'GTS10', '469.48', '469.48', 0, NULL, NULL, NULL, NULL),
(70, 'GEN-INV70', 1, 81, 2, '2017-10-05 06:08:41', '2017-06-08 18:15:00', '1579.20', '157.92', 'GTS10', '1737.12', '1737.12', 0, NULL, NULL, NULL, NULL),
(71, 'GEN-INV71', 1, 82, 2, '2017-10-05 06:08:41', '2017-06-05 18:15:00', '2800.00', '280.00', 'GTS10', '3080.00', '3080.00', 0, NULL, NULL, NULL, NULL),
(72, 'GEN-INV72', 1, 87, 2, '2017-10-05 06:09:46', '2017-06-05 18:15:00', '1000.00', '90.91', 'IncludingGST', '1000.00', '1000.00', 0, NULL, NULL, NULL, NULL),
(73, 'GEN-INV73', 1, 88, 1, '2017-10-05 06:08:41', '2017-06-06 18:15:00', '400.00', '40.00', 'GTS10', '440.00', '440.00', 0, NULL, NULL, NULL, NULL),
(74, 'GEN-INV74', 1, 39, 1, '2017-10-05 06:08:41', '2017-06-27 18:15:00', '7360.00', '736.00', 'GTS10', '8096.00', '8096.00', 0, NULL, NULL, NULL, NULL),
(75, 'GEN-INV75', 1, 39, 1, '2017-10-05 06:08:41', '2017-06-28 18:15:00', '6040.00', '604.00', 'GTS10', '6644.00', '6644.00', 0, NULL, NULL, NULL, NULL),
(76, 'GEN-INV76', 1, 86, 1, '2017-10-05 06:08:41', '2017-07-04 18:15:00', '8635.00', '863.50', 'GTS10', '9498.50', '9498.50', 0, NULL, NULL, NULL, NULL),
(77, 'GEN-INV77', 1, 53, 1, '2017-10-05 06:09:46', '2017-07-05 18:15:00', '4000.00', '363.64', 'IncludingGST', '4000.00', '4000.00', 0, NULL, NULL, NULL, NULL),
(78, 'GEN-INV78', 1, 54, 1, '2017-10-05 06:09:46', '2017-07-05 18:15:00', '3000.00', '272.73', 'IncludingGST', '3000.00', '3000.00', 0, NULL, NULL, NULL, NULL),
(79, 'GEN-INV79', 1, 89, 1, '2017-10-05 06:09:46', '2017-06-29 18:15:00', '5000.00', '454.55', 'IncludingGST', '5000.00', '5000.00', 0, NULL, NULL, NULL, NULL),
(80, 'GEN-INV80', 1, 39, 1, '2017-10-05 06:08:41', '2017-07-24 18:15:00', '7960.00', '796.00', 'GTS10', '8756.00', '8756.00', 0, NULL, NULL, NULL, NULL),
(81, 'GEN-INV81', 1, 46, 1, '2017-10-05 06:08:41', '2017-07-24 18:15:00', '44.00', '4.40', 'GTS10', '48.40', '48.40', 0, NULL, NULL, NULL, NULL),
(82, 'GEN-INV82', 1, 39, 1, '2017-10-05 06:08:41', '2017-08-04 18:15:00', '6040.00', '604.00', 'GTS10', '6644.00', '6644.00', 0, NULL, NULL, NULL, NULL),
(83, 'GEN-INV83', 1, 90, 2, '2017-10-05 06:09:46', '2017-08-02 18:15:00', '654.00', '59.45', 'IncludingGST', '654.00', '654.00', 0, NULL, NULL, NULL, NULL),
(84, 'GEN-INV84', 1, 54, 1, '2017-10-05 06:09:46', '2017-08-10 18:15:00', '4000.00', '363.64', 'IncludingGST', '4000.00', '4000.00', 0, NULL, NULL, NULL, NULL),
(85, 'GEN-INV85', 1, 86, 2, '2017-10-05 06:08:41', '2017-08-05 18:15:00', '1470.00', '147.00', 'GTS10', '1617.00', '1617.00', 0, NULL, NULL, NULL, NULL),
(86, 'GEN-INV86', 1, 93, 2, '2017-10-05 06:08:41', '2017-08-08 18:15:00', '300.00', '30.00', 'GTS10', '330.00', '330.00', 0, NULL, NULL, NULL, NULL),
(87, 'GEN-INV87', 1, 94, 1, '2017-10-05 06:09:46', '2017-08-15 18:15:00', '1200.00', '109.09', 'IncludingGST', '1200.00', '1200.00', 0, NULL, NULL, NULL, NULL),
(88, 'GEN-INV88', 1, 39, 1, '2017-10-05 06:08:41', '2017-08-16 18:15:00', '8480.00', '848.00', 'GTS10', '9328.00', '9328.00', 0, NULL, NULL, NULL, NULL),
(89, 'GEN-INV89', 1, 24, 2, '2017-10-05 06:09:46', '2017-09-04 18:15:00', '580.00', '52.73', 'IncludingGST', '580.00', '580.00', 0, NULL, NULL, NULL, NULL),
(90, 'GEN-INV90', 1, 39, 1, '2017-10-05 06:08:41', '2017-08-24 18:15:00', '7400.00', '740.00', 'GTS10', '8140.00', '8140.00', 0, NULL, NULL, NULL, NULL),
(91, 'GEN-INV91', 1, 95, 2, '2017-10-05 06:08:41', '2017-08-31 18:15:00', '64.00', '6.40', 'GTS10', '70.40', '70.40', 0, NULL, NULL, NULL, NULL),
(92, 'GEN-INV92', 1, 96, 1, '2017-10-05 06:08:41', '2017-09-04 18:15:00', '237.60', '23.76', 'GTS10', '261.36', '261.36', 0, NULL, NULL, NULL, NULL),
(93, 'GEN-INV93', 1, 98, 1, '2017-10-05 06:08:41', '2017-09-04 18:15:00', '619.00', '61.90', 'GTS10', '680.90', '0.00', 1, NULL, NULL, NULL, NULL),
(94, 'GEN-INV94', 1, 97, 1, '2017-10-05 06:08:41', '2017-09-04 18:15:00', '122.00', '12.20', 'GTS10', '134.20', '0.00', 1, NULL, NULL, NULL, NULL),
(95, 'GEN-INV95', 1, 39, 1, '2017-10-05 06:08:41', '2017-09-06 18:15:00', '8760.00', '876.00', 'GTS10', '9636.00', '0.00', 1, NULL, NULL, NULL, NULL),
(96, 'GEN-INV96', 1, 90, 1, '2017-10-05 06:08:41', '2017-09-21 18:15:00', '110.00', '11.00', 'GTS10', '121.00', '121.00', 0, NULL, NULL, NULL, NULL),
(97, 'GEN-INV97', 1, 39, 1, '2017-10-05 06:08:41', '2017-09-24 18:15:00', '7840.00', '784.00', 'GTS10', '8624.00', '8624.00', 0, NULL, NULL, NULL, NULL),
(98, 'GEN-INV98', 1, 97, 1, '2017-10-05 06:08:41', '2017-09-27 18:15:00', '650.00', '65.00', 'GTS10', '715.00', '715.00', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_id`, `invoice_no`, `product_id`, `quantity`, `rate`, `amount`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 2, 'GEN-INV02', 3, '1', '2000.00', '2000.00', 'http://oceaniaeducation.com.au', NULL, NULL, NULL),
(3, 3, 'GEN-INV03', 18, '5.5hrs', '80.00', '440.00', 'Add the additional values in drop-downs : Add given values in dropdowns for Linux, Windows, HPCR and HPC\r\n', NULL, NULL, NULL),
(4, 3, 'GEN-INV03', 18, '3.5hrs', '80.00', '280.00', 'Create a new app for HPC : Create an app for HPC in the portal under Science and Math->HPC\r\n', NULL, NULL, NULL),
(5, 3, 'GEN-INV03', 18, '1hr', '80.00', '80.00', 'Additional Support :Support as well as removing and formatting existing data', NULL, NULL, NULL),
(6, 4, 'GEN-INV04', 19, '1', '1700.00', '1700.00', '', NULL, NULL, NULL),
(7, 5, 'GEN-INV05', 20, '32hrs', '40.00', '1280.00', 'Role Entry /Access Control Development ', NULL, NULL, NULL),
(8, 5, 'GEN-INV05', 20, '28hrs', '40.00', '1120.00', 'Role Menu Design & development', NULL, NULL, NULL),
(9, 5, 'GEN-INV05', 20, '42hrs', '40.00', '1680.00', 'Destination DBMS and Development', NULL, NULL, NULL),
(10, 5, 'GEN-INV05', 20, '30hrs', '40.00', '1200.00', 'Booking System Integrated ', NULL, NULL, NULL),
(11, 5, 'GEN-INV05', 20, '8hrs', '40.00', '320.00', 'Testing, debugging and fixing bugs', NULL, NULL, NULL),
(12, 6, 'GEN-INV06', 4, '1', '4000.00', '4000.00', 'bnbconsulting.com.au', NULL, NULL, NULL),
(13, 6, 'GEN-INV06', 9, '1', '270.00', '270.00', 'Set Up Email and Configuration', NULL, NULL, NULL),
(14, 6, 'GEN-INV06', 13, '1', '280.00', '280.00', 'Added meta tag to website and regular maintainance', NULL, NULL, NULL),
(15, 7, 'GEN-INV07', 11, '6hrs', '60.00', '360.00', 'Front Desk Computer and Director PC', NULL, NULL, NULL),
(16, 7, 'GEN-INV07', 9, '6hrs', '60.00', '360.00', 'Configuration Emails :info and namaste', NULL, NULL, NULL),
(17, 7, 'GEN-INV07', 16, '1', '435.00', '435.00', 'Commercial photshot', NULL, NULL, NULL),
(18, 8, 'GEN-INV08', 21, '1', '900.00', '900.00', 'Smarteducationvisa.com.au', NULL, NULL, NULL),
(21, 9, 'GEN-INV09', 5, '3hrs', '40.00', '120.00', 'Dell Laptop :Format and Applications Installation', NULL, NULL, NULL),
(22, 9, 'GEN-INV09', 5, '1', '360.00', '360.00', 'HP Laptop:Screen Repait and HD Replaced ,RAM Replaced', NULL, NULL, NULL),
(23, 12, 'GEN-INV12', 3, '50hrs', '40.00', '2000.00', 'Research Planner System development - partial payments', NULL, NULL, NULL),
(24, 13, 'GEN-INV13', 11, '3', '80.00', '240.00', '', NULL, NULL, NULL),
(25, 13, 'GEN-INV13', 8, '3', '120.00', '360.00', 'Replaced RAM .SD CARD, HD', NULL, NULL, NULL),
(26, 13, 'GEN-INV13', 17, '5', '80.00', '400.00', 'Remote IT Support 23/11', NULL, NULL, NULL),
(27, 14, 'GEN-INV14', 11, '2', '40.00', '80.00', '', NULL, NULL, NULL),
(28, 14, 'GEN-INV14', 5, '1', '348.00', '348.00', '', NULL, NULL, NULL),
(29, 14, 'GEN-INV14', 4, '1', '1300.00', '1300.00', '', NULL, NULL, NULL),
(30, 15, 'GEN-INV15', 21, '1', '1500.00', '1500.00', 'Cleaning quote system development', NULL, NULL, NULL),
(31, 16, 'GEN-INV16', 17, '100', '40.00', '4000.00', 'Worked on Asset Group Solutions Site ', NULL, NULL, NULL),
(32, 17, 'GEN-INV17', 8, '2', '130.00', '260.00', 'Lenovo computer ( Upgraded RAM, HD)', NULL, NULL, NULL),
(33, 17, 'GEN-INV17', 7, '1', '40.00', '40.00', '', NULL, NULL, NULL),
(34, 17, 'GEN-INV17', 6, '1', '210.00', '210.00', '', NULL, NULL, NULL),
(35, 17, 'GEN-INV17', 5, '9', '40.00', '360.00', '', NULL, NULL, NULL),
(36, 18, 'GEN-INV18', 3, '145', '40.00', '5800.00', 'Stock take system development : DBMS management, Use case diagram, Sequence diagram, front end development', NULL, NULL, NULL),
(37, 19, 'GEN-INV19', 4, '1', '3650.00', '3650.00', 'http://www.namastespices.com.au/', NULL, NULL, NULL),
(38, 20, 'GEN-INV20', 7, '1', '22.73', '22.73', 'fifthbadsector.com', NULL, NULL, NULL),
(39, 20, 'GEN-INV20', 6, '1', '250.00', '250.00', 'fifthbadsector.com ( 1 year)', NULL, NULL, NULL),
(40, 20, 'GEN-INV20', 21, '1', '2000.00', '2000.00', 'web design of fifthbadsector.com', NULL, NULL, NULL),
(41, 21, 'GEN-INV21', 21, '1', '950.00', '950.00', '', NULL, NULL, NULL),
(42, 21, 'GEN-INV21', 17, '1', '275.00', '275.00', 'Computer Setup/ Networks', NULL, NULL, NULL),
(43, 21, 'GEN-INV21', 17, '1', '140.00', '140.00', 'Laptop Repair- Acer', NULL, NULL, NULL),
(44, 22, 'GEN-INV22', 5, '3', '150.00', '450.00', '', NULL, NULL, NULL),
(45, 22, 'GEN-INV22', 19, '1', '310.00', '310.00', '', NULL, NULL, NULL),
(46, 22, 'GEN-INV22', 11, '3', '50.00', '150.00', '', NULL, NULL, NULL),
(47, 23, 'GEN-INV23', 5, '1', '500.00', '500.00', 'Mac book Pro Repair, Installed software and MS OFFICE', NULL, NULL, NULL),
(48, 24, 'GEN-INV24', 3, '170hrs', '40.00', '6800.00', 'Stocktake Management system : Coding, Testing,Debugging and Added other features', NULL, NULL, NULL),
(49, 25, 'GEN-INV25', 5, '1', '240.00', '240.00', 'Mac book Repair and software development', NULL, NULL, NULL),
(50, 25, 'GEN-INV25', 17, '1', '215.00', '215.00', 'Remote IT Support', NULL, NULL, NULL),
(51, 26, 'GEN-INV26', 3, '102hrs', '40.00', '4080.00', 'stock.geniusits.com ( Unifrom stockttake system development)', NULL, NULL, NULL),
(52, 27, 'GEN-INV27', 8, '1', '1000.00', '1000.00', 'Lenovo Laptop and Application / Office Installation', NULL, NULL, NULL),
(53, 28, 'GEN-INV28', 3, '225hrs', '40.00', '9000.00', 'market.geniusits.com:\r\n- Mock up Design\r\n- Front End Development\r\n- Business Analysis\r\n- Database Design\r\n- Coding - Alpha Version ( MVC Framework)', NULL, NULL, NULL),
(54, 29, 'GEN-INV29', 22, '2 yrs', '14.50', '29.00', 'Domain Renewal of himalayanasiatours.com.au for 2 yrs', NULL, NULL, NULL),
(55, 30, 'GEN-INV30', 21, '1', '1640.00', '1640.00', 'Responsive webdesign and Hosted on Cloud ', NULL, NULL, NULL),
(56, 30, 'GEN-INV30', 3, '1', '2000.00', '2000.00', 'Client Compliance Database System', NULL, NULL, NULL),
(57, 32, 'GEN-INV32', 16, '1', '250.00', '250.00', 'Photos for Nepali book', NULL, NULL, NULL),
(58, 32, 'GEN-INV32', 15, '1', '250.00', '250.00', 'Short Video about Nepali alphabet', NULL, NULL, NULL),
(59, 33, 'GEN-INV33', 17, '18hrs', '40.00', '720.00', 'Computer Networks:Support LAN/WAN,Network Segments ,Internet and Intranet System', NULL, NULL, NULL),
(60, 33, 'GEN-INV33', 9, '16hrs', '40.00', '640.00', 'Administer  desktop computers, printers, routers, switches, firewalls.', NULL, NULL, NULL),
(61, 34, 'GEN-INV34', 21, '1', '1000.00', '1000.00', 'Portal website development for eva fashion', NULL, NULL, NULL),
(62, 35, 'GEN-INV35', 21, '1', '1000.00', '1000.00', 'Online website development', NULL, NULL, NULL),
(63, 36, 'GEN-INV36', 17, '1', '500.00', '500.00', 'Acer Laptop Repair and Application Installation', NULL, NULL, NULL),
(64, 37, 'GEN-INV37', 8, '1', '360.00', '360.00', 'Upgraded RAM ,HD Replacement adn Charger Replacement', NULL, NULL, NULL),
(65, 37, 'GEN-INV37', 17, '1', '250.00', '250.00', 'Software / Application Installation', NULL, NULL, NULL),
(66, 37, 'GEN-INV37', 17, '1', '300.00', '300.00', 'Router Configuration, Computer Networks and LAN /WAN Setup', NULL, NULL, NULL),
(67, 38, 'GEN-INV38', 18, '2hrs', '80.00', '160.00', 'Meeting and Support :On Site Support and Meeting on 13/12/2016\r\n\r\n', NULL, NULL, NULL),
(68, 38, 'GEN-INV38', 18, '2hrs', '80.00', '160.00', 'Add new instance: add the instance types p2.xlarge and p2.8xlarge to the portal as options for the â€œR Statistical Computingâ€ and â€œHPCâ€ options under Science and Math\r\n\r\n', NULL, NULL, NULL),
(69, 38, 'GEN-INV38', 18, '2hrs', '80.00', '160.00', 'Update sql Query:update query and add p2.xlarge and p2.8xlarge only for \"Linux Instance Provisioning\" under General Medicine? also please remove p2.xlarge, p2.8xlarge from \"R Statistical Computing\" and \"HPC\" options\r\n\r\n:\r\n', NULL, NULL, NULL),
(70, 38, 'GEN-INV38', 18, '2hrs', '80.00', '160.00', 'dd g2.2xlarge and g2.8xlarge for \"Alces flight-r Shiny Server\" and \"HPC\" under Science and Math\r\n', NULL, NULL, NULL),
(71, 38, 'GEN-INV38', 18, '2hrs', '80.00', '160.00', 'Remote support, assistance and training given regarding the HPC portal\r\n', NULL, NULL, NULL),
(72, 39, 'GEN-INV39', 17, '1', '200.00', '200.00', 'Laptop repairs, Installation of software, ', NULL, NULL, NULL),
(73, 39, 'GEN-INV39', 11, '1', '80.00', '80.00', '', NULL, NULL, NULL),
(74, 39, 'GEN-INV39', 17, '1', '175.00', '175.00', 'Router and Network Configuration and installation', NULL, NULL, NULL),
(75, 40, 'GEN-INV40', 3, '225hrs', '40.00', '9000.00', 'STOCK MANAGEMENT SYSTEM DEVELOPMENT ', NULL, NULL, NULL),
(76, 41, 'GEN-INV41', 17, '1', '250.00', '250.00', 'Email setting/router network set up', NULL, NULL, NULL),
(77, 42, 'GEN-INV42', 16, '1', '500.00', '500.00', 'Couple photoshot', NULL, NULL, NULL),
(78, 43, 'GEN-INV43', 30, '1', '550.00', '550.00', 'CPU, Monitor, Wireless Keyboard and Mouse, USB ', NULL, NULL, NULL),
(79, 44, 'GEN-INV44', 5, '1', '250.00', '250.00', 'Laptop repair and office installation ', NULL, NULL, NULL),
(80, 45, 'GEN-INV45', 14, '80', '40.00', '3200.00', 'market.slairsystem.com.au:\r\nAdded new features : Item/ Record / Sample', NULL, NULL, NULL),
(81, 46, 'GEN-INV46', 21, '1', '900.00', '900.00', 'Cleaning/Pest Control website development', NULL, NULL, NULL),
(82, 47, 'GEN-INV47', 4, '1', '1800.00', '1800.00', 'it support', NULL, NULL, NULL),
(83, 48, 'GEN-INV48', 21, '1', '1520.00', '1520.00', 'it support', NULL, NULL, NULL),
(84, 49, 'GEN-INV49', 19, '1', '650.00', '650.00', 'elevategroupservices.com.au', NULL, NULL, NULL),
(85, 49, 'GEN-INV49', 19, '1', '750.00', '750.00', 'elevategroupservices.com.au', NULL, NULL, NULL),
(86, 49, 'GEN-INV49', 21, '70hrs', '40.00', '2800.00', 'Mockup- Front End - CMS', NULL, NULL, NULL),
(87, 49, 'GEN-INV49', 14, '160', '40.00', '6400.00', 'market system new features updates', NULL, NULL, NULL),
(88, 50, 'GEN-INV50', 21, '1', '3200.00', '3200.00', 'Elevate Web development , Logo Design and website design', NULL, NULL, NULL),
(89, 50, 'GEN-INV50', 3, '171.50', '40.00', '6860.00', 'Market : Software Upgrade', NULL, NULL, NULL),
(90, 51, 'GEN-INV51', 17, '1', '500.00', '500.00', '', NULL, NULL, NULL),
(91, 52, 'GEN-INV52', 17, '1', '100.00', '100.00', 'call out and service fee', NULL, NULL, NULL),
(92, 53, 'GEN-INV53', 17, '1', '120.00', '120.00', ' Domain purchase and hosting for 2 years', NULL, NULL, NULL),
(93, 54, 'GEN-INV54', 18, '8hrs', '80.00', '640.00', 'List of Instances: Added new Instances tab in menu and renamed old instances to stacks\r\n', NULL, NULL, NULL),
(94, 54, 'GEN-INV54', 18, '8hrs', '80.00', '640.00', 'Create Backup :Added feature to create backup, Number of changes and patching were done based on Vineelâ€™s request.\r\n\r\n', NULL, NULL, NULL),
(95, 54, 'GEN-INV54', 18, '8hrs', '80.00', '640.00', 'Backup Instances: Added new tab named Backup Instances that will list the backup instances\r\n\r\n', NULL, NULL, NULL),
(96, 54, 'GEN-INV54', 18, '8hrs', '80.00', '640.00', 'Launch Instance: Added feature to launch backup instance. Number of changes and patching were done based on Vineelâ€™s request.\r\n\r\n', NULL, NULL, NULL),
(97, 55, 'GEN-INV55', 23, '1', '549.00', '549.00', 'Kogan TV 49\" 4K Smart (Ultra HD Tv)', NULL, NULL, NULL),
(98, 55, 'GEN-INV55', 23, '1', '768.00', '768.00', 'smart view cctv and apple tv supply', NULL, NULL, NULL),
(99, 55, 'GEN-INV55', 17, '12hrs', '40.00', '480.00', 'Configuration of Router, Set Up Dual Monitor (HD TV),Apple TV Support, ', NULL, NULL, NULL),
(100, 56, 'GEN-INV56', 3, '178hrs', '40.00', '7120.00', 'MARKET SYSTEM : Added few more features for GATE3 and POTTS Street', NULL, NULL, NULL),
(101, 57, 'GEN-INV57', 2, '1', '35.00', '35.00', 'Righton Finance Logo Design', NULL, NULL, NULL),
(102, 58, 'GEN-INV58', 21, '1', '2900.00', '2900.00', 'imaxpropertyservices.com.au', NULL, NULL, NULL),
(103, 59, 'GEN-INV59', 3, '178', '40.00', '7120.00', 'Job card system : Mockup, Flowchart,system analysis, Front End development in MVC Framework and GIT ', NULL, NULL, NULL),
(104, 60, 'GEN-INV60', 18, '2', '40.00', '80.00', 'List of stacks: Helped correctly listing the stacks from AWS End, and sorted the fields in web portal\r\n', NULL, NULL, NULL),
(105, 60, 'GEN-INV60', 18, '1', '40.00', '40.00', 'Delete backup instances:Added routine to delete backup instance\r\n\r\n', NULL, NULL, NULL),
(106, 60, 'GEN-INV60', 18, '3', '40.00', '120.00', 'Start/Stop features:Added routine to start and stop instances based on state of the instance\r\n', NULL, NULL, NULL),
(107, 60, 'GEN-INV60', 18, '1', '40.00', '40.00', 'Cosmetic UI changes : Did some layout and UI cosmeti changes\r\n\r\n', NULL, NULL, NULL),
(108, 61, 'GEN-INV61', 7, '1', '22.80', '22.80', 'agfsconnect.com ', NULL, NULL, NULL),
(109, 61, 'GEN-INV61', 7, '1', '28.80', '28.80', 'agfsconnect.com.au', NULL, NULL, NULL),
(110, 61, 'GEN-INV61', 1, '1', '700.00', '700.00', 'agfsconnect.com.au (With 40% Discount)', NULL, NULL, NULL),
(111, 61, 'GEN-INV61', 17, '0', '0.00', '0.00', 'Email Setting and IT Support : FREE', NULL, NULL, NULL),
(112, 62, 'GEN-INV62', 7, '1', '20.00', '20.00', 'a1groupservices.com.au(1year)', NULL, NULL, NULL),
(113, 62, 'GEN-INV62', 1, '1', '600.00', '600.00', 'a1groupservices.com.au ( 50% discount)', NULL, NULL, NULL),
(114, 63, 'GEN-INV63', 3, '157', '40.00', '6280.00', 'Job Card System : User Creation/Manage, Building Add/Manage, Access Control as user : Admin, Manager,Supervisor,Client and Employee,\r\n', NULL, NULL, NULL),
(115, 64, 'GEN-INV64', 19, '1', '220.00', '220.00', 'Staff Management System', NULL, NULL, NULL),
(116, 64, 'GEN-INV64', 7, '1', '100.00', '100.00', 'Domain and Hosting', NULL, NULL, NULL),
(117, 64, 'GEN-INV64', 3, '142', '40.00', '5680.00', 'Design, Development back end system', NULL, NULL, NULL),
(118, 65, 'GEN-INV65', 21, '1', '1400.00', '1400.00', 'D&E Front End  Development / Back-end Development', NULL, NULL, NULL),
(119, 66, 'GEN-INV66', 21, '1', '700.00', '700.00', 'rightonfinance.com.au website development with CMS', NULL, NULL, NULL),
(120, 67, 'GEN-INV67', 23, '1', '1131.00', '1131.00', 'Lenovo Laptop , Office Installation , Antivirus Installation and Installed other required applications.', NULL, NULL, NULL),
(121, 67, 'GEN-INV67', 17, '1', '300.00', '300.00', 'Provided IT Support ( Email configuration, Hosting management and Routing Configuration)', NULL, NULL, NULL),
(122, 68, 'GEN-INV68', 3, '356', '40.00', '14240.00', 'Stock take system Upgrade , Warehouse Mgmt system', NULL, NULL, NULL),
(123, 69, 'GEN-INV69', 7, '2', '38.40', '76.80', 'linkpeople.uk and linkpeople.co.uk', NULL, NULL, NULL),
(124, 69, 'GEN-INV69', 6, '1', '110.00', '110.00', 'Linux Hosting - linkpeople.com.au for 1 Years', NULL, NULL, NULL),
(125, 69, 'GEN-INV69', 17, '8hrs', '30.00', '240.00', 'Email Migration (Employee hours only)', NULL, NULL, NULL),
(126, 70, 'GEN-INV70', 21, '50hrs', '30.00', '1500.00', 'Employee hours ( WITH DISCOUNTED RATE)', NULL, NULL, NULL),
(127, 70, 'GEN-INV70', 24, '2', '39.60', '79.20', 'Istock image purchased', NULL, NULL, NULL),
(128, 71, 'GEN-INV71', 25, '1', '200.00', '200.00', '', NULL, NULL, NULL),
(129, 71, 'GEN-INV71', 3, '65', '40.00', '2600.00', 'User Management, User access control, Roster of  staff and balance sheet.', NULL, NULL, NULL),
(130, 72, 'GEN-INV72', 23, '1', '1000.00', '1000.00', 'Toshiba Laptop with accessories , Other porgrammes/Applications Installation, Email Configuration', NULL, NULL, NULL),
(131, 73, 'GEN-INV73', 1, '1', '400.00', '400.00', '3streets.com.au', NULL, NULL, NULL),
(132, 74, 'GEN-INV74', 3, '184hrs', '40.00', '7360.00', 'Employee Management System development', NULL, NULL, NULL),
(133, 75, 'GEN-INV75', 3, '120hrs', '40.00', '4800.00', 'Warehouse Management System ', NULL, NULL, NULL),
(134, 75, 'GEN-INV75', 3, '31hrs', '40.00', '1240.00', 'Upgrade Market system and Stock system ', NULL, NULL, NULL),
(135, 76, 'GEN-INV76', 25, '3', '45.00', '135.00', '', NULL, NULL, NULL),
(136, 76, 'GEN-INV76', 23, '1', '150.00', '150.00', '', NULL, NULL, NULL),
(137, 76, 'GEN-INV76', 21, '1', '3600.00', '3600.00', 'dneinvestment.com.au web protal development', NULL, NULL, NULL),
(138, 76, 'GEN-INV76', 11, '3', '150.00', '450.00', '', NULL, NULL, NULL),
(139, 76, 'GEN-INV76', 3, '107.5hrs', '40.00', '4300.00', 'Accounting Software: Mock up design, Coding, Features including.', NULL, NULL, NULL),
(140, 77, 'GEN-INV77', 1, '1', '4000.00', '4000.00', 'Travel Website development', NULL, NULL, NULL),
(141, 78, 'GEN-INV78', 23, '5', '600.00', '3000.00', '', NULL, NULL, NULL),
(142, 79, 'GEN-INV79', 3, '125hrs', '40.00', '5000.00', 'Tax Return System Development', NULL, NULL, NULL),
(144, 81, 'GEN-INV81', 27, '8 months', '5.50', '44.00', 'bestchoicetravels.com.au domain renewal fee', NULL, NULL, NULL),
(145, 82, 'GEN-INV82', 7, '8 months', '5.50', '44.00', 'bestchoicetravel.com.au domain renewal fee', NULL, NULL, NULL),
(147, 80, 'GEN-INV80', 3, '199hrs', '40.00', '7960.00', 'Upgrade stock take system and Market system\r\nMock up Design of PO system ,Business Analysis /System Analysis ,Front End Design', NULL, NULL, NULL),
(148, 82, 'GEN-INV82', 3, '151', '40.00', '6040.00', 'Staff KPI Development for ACU', NULL, NULL, NULL),
(149, 83, 'GEN-INV83', 13, '1', '654.00', '654.00', 'Search Engine Optimization Done and IT Consulting Fee', NULL, NULL, NULL),
(150, 84, 'GEN-INV84', 3, '1', '4000.00', '4000.00', '', NULL, NULL, NULL),
(151, 85, 'GEN-INV85', 30, '1', '450.00', '450.00', 'Moniter, Mouse, Keyboard, CPU and cables', NULL, NULL, NULL),
(155, 85, 'GEN-INV85', 31, '1', '90.00', '90.00', '', NULL, NULL, NULL),
(156, 85, 'GEN-INV85', 32, '1', '30.00', '30.00', 'vodafone  sim 1 months data only', NULL, NULL, NULL),
(157, 86, 'GEN-INV86', 1, '1', '200.00', '200.00', 'safenetsecurity.com.au/', NULL, NULL, NULL),
(158, 86, 'GEN-INV86', 6, '1', '100.00', '100.00', '1 year', NULL, NULL, NULL),
(159, 87, 'GEN-INV87', 3, '40hrs', '30.00', '1200.00', 'Tax Return System development :\r\nTax Offer, Ticket ID, Email Confirmation, Mark Check-boxes,Listing Users', NULL, NULL, NULL),
(160, 88, 'GEN-INV88', 3, '212hrs', '40.00', '8480.00', 'Software Development for Asset Site', NULL, NULL, NULL),
(161, 89, 'GEN-INV89', 30, '1', '580.00', '580.00', 'mouse, keyboard, CPU ,Cables,monitor', NULL, NULL, NULL),
(162, 90, 'GEN-INV90', 3, '185hrs', '40.00', '7400.00', '', NULL, NULL, NULL),
(163, 91, 'GEN-INV91', 33, '800words', '0.08', '64.00', 'for website : roomtoroad.com.au', NULL, NULL, NULL),
(164, 92, 'GEN-INV92', 34, '2 yrs', '25.80', '51.60', 'teamconsult.com and teamconsult.com.au', NULL, NULL, NULL),
(165, 92, 'GEN-INV92', 35, '2 yrs', '93.00', '186.00', '', NULL, NULL, NULL),
(166, 92, 'GEN-INV92', 17, '0', '0.00', '0.00', 'free', NULL, NULL, NULL),
(167, 93, 'GEN-INV93', 36, '1 year', '120.00', '120.00', '', NULL, NULL, NULL),
(168, 93, 'GEN-INV93', 21, '1', '499.00', '499.00', '', NULL, NULL, NULL),
(169, 94, 'GEN-INV94', 37, '1 yrs', '122.00', '122.00', '(sharpeyessecurity,sharpeyesgroupsolutions,sharpeyesglobaltransport)', NULL, NULL, NULL),
(170, 85, 'GEN-INV85', 17, '3', '300.00', '900.00', 'IT Support for 3 months', NULL, NULL, NULL),
(171, 95, 'GEN-INV95', 3, '219', '40.00', '8760.00', 'Docs Feeder System Design , Coding, Hosting \r\nDatabase Entry from previous System \r\n', NULL, NULL, NULL),
(172, 96, 'GEN-INV96', 35, '1', '110.00', '110.00', '1 YEAR HOSTING for roamingpestcontrol.com.au', NULL, NULL, NULL),
(173, 97, 'GEN-INV97', 3, '196', '40.00', '7840.00', 'Docs Feeder system development', NULL, NULL, NULL),
(174, 98, 'GEN-INV98', 21, '1', '500.00', '500.00', 'Website Development ozzies Group Solutions', NULL, NULL, NULL),
(175, 98, 'GEN-INV98', 33, '1', '150.00', '150.00', 'Content Writing for website', NULL, NULL, NULL),
(176, 98, 'GEN-INV98', 2, '1', '0.00', '0.00', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_08_08_080258_create_suppliers_table', 1),
(13, '2017_08_08_092630_create_products_table', 1),
(14, '2017_08_08_103614_create_customers_table', 1),
(15, '2017_08_10_061501_create_payments_table', 1),
(16, '2017_08_15_064003_create_invoices_table', 1),
(17, '2017_09_17_074713_create_invoice_details_table', 1),
(19, '2017_09_20_093544_create_payamounts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payamounts`
--

CREATE TABLE `payamounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payamounts`
--

INSERT INTO `payamounts` (`id`, `invoice_id`, `invoice_no`, `paid_amount`, `paid_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2', 'GEN-INV02', '2000.00', '2016-11-08 18:15:00', NULL, NULL, NULL),
(2, '5', 'GEN-INV05', '6125.00', '2016-11-09 18:15:00', NULL, NULL, NULL),
(3, '4', 'GEN-INV04', '1870.00', '2016-11-09 18:15:00', NULL, NULL, NULL),
(4, '7', 'GEN-INV07', '1270.00', '2016-11-09 18:15:00', NULL, NULL, NULL),
(5, '8', 'GEN-INV08', '990.00', '2016-11-09 18:15:00', NULL, NULL, NULL),
(6, '6', 'GEN-INV06', '4000.00', '2016-11-16 18:15:00', NULL, NULL, NULL),
(7, '6', 'GEN-INV06', '1000.00', '2016-11-17 18:15:00', NULL, NULL, NULL),
(8, '13', 'GEN-INV13', '1100.00', '2016-11-23 18:15:00', NULL, NULL, NULL),
(9, '15', 'GEN-INV15', '1500.00', '2016-11-23 18:15:00', NULL, NULL, NULL),
(10, '61', 'GEN-INV61', '286.76', '2017-07-17 18:15:00', NULL, NULL, NULL),
(11, '61', 'GEN-INV61', '540.00', '2017-07-18 18:15:00', NULL, NULL, NULL),
(12, '95', 'GEN-INV95', '9636.00', '2017-08-31 18:15:00', NULL, NULL, NULL),
(13, '94', 'GEN-INV94', '134.20', '2017-08-28 18:15:00', NULL, NULL, NULL),
(14, '93', 'GEN-INV93', '680.90', '2017-09-15 18:15:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bsb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pay_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Direct Bank Deposit',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `supplier_id`, `bank_name`, `bsb`, `account_no`, `pay_method`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Commonwealth bank', '062265', '1125 4493', 'Banking Details', 1, NULL, NULL, NULL),
(2, 2, 'Commonwealth bank', '062265', '1125 4493', 'Banking Details', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Website Design', NULL, NULL, NULL, 1),
(2, 'Logo Design', NULL, NULL, NULL, 1),
(3, 'Software Development', NULL, NULL, NULL, 1),
(4, 'E-commerce web development', NULL, NULL, NULL, 1),
(5, 'Compter Repair and Maintainance', NULL, NULL, NULL, 1),
(6, 'Web Hosting', NULL, NULL, NULL, 1),
(7, 'Domain Registration', NULL, NULL, NULL, 1),
(8, 'Hardware Services', NULL, NULL, NULL, 1),
(9, 'Remote Monitoring and Maintainance', NULL, NULL, NULL, 1),
(10, 'Server Verualizationa and Installation', NULL, NULL, NULL, 1),
(11, 'Back up and Disaster recovery', NULL, NULL, NULL, 1),
(12, 'Data Networking', NULL, NULL, NULL, 1),
(13, 'SEO - Online Marketing', NULL, NULL, NULL, 1),
(14, 'App Developement', NULL, NULL, NULL, 1),
(15, 'Videography', NULL, NULL, NULL, 1),
(16, 'Photography', NULL, NULL, NULL, 1),
(17, 'IT Support', NULL, NULL, NULL, 1),
(18, 'HPC Portal', NULL, NULL, NULL, 1),
(19, 'System Analysis Consulting fee', NULL, NULL, NULL, 1),
(20, 'BCT Travel System', NULL, NULL, NULL, 1),
(21, 'Website Development', NULL, NULL, NULL, 1),
(22, 'Renewal Service', NULL, NULL, NULL, 1),
(23, 'Computer and Electronics Item', NULL, NULL, NULL, 1),
(24, 'Image Purchased', NULL, NULL, NULL, 1),
(25, 'Flyer Design', NULL, NULL, NULL, 1),
(26, 'PC/Laptop Clean up', NULL, NULL, NULL, 1),
(27, 'Domain Renewal', NULL, NULL, NULL, 1),
(28, 'BAS Preparation', NULL, NULL, NULL, 1),
(29, 'Ac Fee', NULL, NULL, NULL, 1),
(30, 'PC Set', NULL, NULL, NULL, 1),
(31, 'Internet Dongle', NULL, NULL, NULL, 1),
(32, 'Voda Sim', NULL, NULL, NULL, 1),
(33, 'Content Writing', NULL, NULL, NULL, 1),
(34, 'Domain Registration', NULL, NULL, NULL, 1),
(35, 'Linux Hosting', NULL, NULL, NULL, 1),
(36, 'Domain Registration and Hosting', NULL, NULL, NULL, 1),
(37, 'Domain Hosting Renewal', NULL, NULL, '2017-10-05 00:57:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplier_name`, `abn`, `logo`, `email`, `phone1`, `phone2`, `website`, `street`, `city`, `state`, `zip_code`, `country`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Genius IT Solutions Pty Ltd', '39168814999', 'image_662646.png', 'accounts@geniusitsolutions.com.au', '02-8740 6353', '0421 673 915', 'www.geniusitsolutions.com.au', '5B/186-188 Canterbury Road', 'Canterbury', 'NSW', '2193', 'Australia', 1, NULL, '2017-10-05 01:52:03'),
(2, 'Genius IT Solutions T/As Genius Media Group', '39168814999', 'image_231983.PNG', 'accounts@geniusitsolutions.com.au', '+61421673915', NULL, 'www.geniusmediagroup.com.au', 'Suite 1406, Level 14, 447 Kent Street', 'Sydney', 'NSW', '2000', 'Australia', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'invoice', 'invoice@invoice.com', '$2y$10$oRpDc0H4HC8QL6YjMtBfL.i6uEHyMF4P5tzLX10gSbiW3/grvqc4a', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_invoice_no_unique` (`invoice_no`),
  ADD KEY `invoices_supplier_id_foreign` (`supplier_id`),
  ADD KEY `invoices_customer_id_foreign` (`customer_id`),
  ADD KEY `invoices_payment_id_foreign` (`payment_id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_details_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_details_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payamounts`
--
ALTER TABLE `payamounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `payamounts`
--
ALTER TABLE `payamounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoice_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
