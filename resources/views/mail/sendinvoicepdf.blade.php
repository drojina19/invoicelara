@extends('mail.master')
@section('main-section')
    Dear {{$invoice->customer->name}},<br><br>
    @if($invoice->status==0)
        Please find the invoice attached.<br><br><br><br>
        We appreciate your prompt payment.<br><br><br><br>
        Please send us the payment receipt once completed to this email.<br><br><br><br>
    @else
        Thank you for making payment.<br><br>
        Please find attahed receipt for the same.<br><br><br>
    @endif
        Thank you for doing business with us.<br><br>
        {{$invoice->supplier->supplier_name}}<br><br>
        Accounts Team


@stop
