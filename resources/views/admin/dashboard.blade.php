@extends('admin.baselayout.baselayout')
@section('custom-css')
    <style>
        .design
        {
            font-weight: bold;
            font-size: 18px;
            text-align: right;

        }
        p.alert{
            padding-left: 5px;
        }
    </style>
@endsection
@section('main-content')
    <div class="">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-files-o" style="color:#4da5df"></i></div>
                    <div class="count">{{count($invoices)}}</div>
                    <h3>Total Invoices</h3>
                    <p class=" alert alert-info">
                        <span class="design">
                            $ {{number_format($invoices->sum('grandtotal'),'2','.',',')}}
                        </span>
                        <span class="amt-label">&nbsp;(Total Amount)</span>
                    </p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-check-square-o" style="color:#40c2a6"></i></div>
                    <div class="count">{{count($invoices->where('status',1))}}</div>
                    <h3>Paid Invoices</h3>
                    <p class="alert alert-success">
                        <span class="design">
                            $ {{number_format($invoices->where('status',1)->sum('grandtotal','-','amt_owed'),'2','.',',')}}
                        </span>
                        <span class="amt-label">&nbsp;(Total Amount)</span>
                    </p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-times" style="color:rgba(231, 76, 60, 0.88)"></i></div>
                    <div class="count">{{count($invoices->where('status',0))}}</div>
                    <h3>Unpaid Invoices</h3>
                    <p class="alert alert-danger">
                        <span class="design">
                            $ {{number_format($invoices->where('status',0)->sum('amt_owed'),'2','.',',')}}
                        </span>
                        <span class="amt-label">&nbsp;(TotalAmount)</span>
                    </p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-clock-o" style="color:rgba(243, 156, 18, 0.88)"></i></div>
                    <div class="count">{{count($invoices->where('status',0)->where("due_date","<",date('Y-m-d')))}}</div>
                    <h3>Overdue Invoices</h3>
                    <p class="alert alert-warning">
                        <span class="design">
                            $ {{number_format($invoices->where('status',0)->where("due_date","<",date('Y-m-d'))->sum('amt_owed'),'2','.',',')}}
                        </span>
                        <span class="amt-label">&nbsp;(Total Amount)</span>
                    </p>
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Transaction Summary <small>Weekly progress</small></h2>--}}
                        {{--<div class="filter">--}}
                            {{--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">--}}
                                {{--<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>--}}
                                {{--<span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}
                        {{--<div class="col-md-9 col-sm-12 col-xs-12">--}}
                            {{--<div class="demo-container" style="height:280px">--}}
                                {{--<div id="placeholder33x" class="demo-placeholder"></div>--}}
                            {{--</div>--}}
                            {{--<div class="tiles">--}}
                                {{--<div class="col-md-4 tile">--}}
                                    {{--<span>Total Sessions</span>--}}
                                    {{--<h2>231,809</h2>--}}
                          {{--<span class="sparkline11 graph" style="height: 160px;">--}}
                                          {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                      {{--</span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 tile">--}}
                                    {{--<span>Total Revenue</span>--}}
                                    {{--<h2>$231,809</h2>--}}
                          {{--<span class="sparkline22 graph" style="height: 160px;">--}}
                                          {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                      {{--</span>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4 tile">--}}
                                    {{--<span>Total Sessions</span>--}}
                                    {{--<h2>231,809</h2>--}}
                          {{--<span class="sparkline11 graph" style="height: 160px;">--}}
                                          {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                      {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        {{--</div>--}}

                        {{--<div class="col-md-3 col-sm-12 col-xs-12">--}}
                            {{--<div>--}}
                                {{--<div class="x_title">--}}
                                    {{--<h2>Top Profiles</h2>--}}
                                    {{--<ul class="nav navbar-right panel_toolbox">--}}
                                        {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="dropdown">--}}
                                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                            {{--<ul class="dropdown-menu" role="menu">--}}
                                                {{--<li><a href="#">Settings 1</a>--}}
                                                {{--</li>--}}
                                                {{--<li><a href="#">Settings 2</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                                {{--<ul class="list-unstyled top_profiles scroll-view">--}}
                                    {{--<li class="media event">--}}
                                        {{--<a class="pull-left border-aero profile_thumb">--}}
                                            {{--<i class="fa fa-user aero"></i>--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<a class="title" href="#">Ms. Mary Jane</a>--}}
                                            {{--<p><strong>$2300. </strong> Agent Avarage Sales </p>--}}
                                            {{--<p> <small>12 Sales Today</small>--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li class="media event">--}}
                                        {{--<a class="pull-left border-green profile_thumb">--}}
                                            {{--<i class="fa fa-user green"></i>--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<a class="title" href="#">Ms. Mary Jane</a>--}}
                                            {{--<p><strong>$2300. </strong> Agent Avarage Sales </p>--}}
                                            {{--<p> <small>12 Sales Today</small>--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li class="media event">--}}
                                        {{--<a class="pull-left border-blue profile_thumb">--}}
                                            {{--<i class="fa fa-user blue"></i>--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<a class="title" href="#">Ms. Mary Jane</a>--}}
                                            {{--<p><strong>$2300. </strong> Agent Avarage Sales </p>--}}
                                            {{--<p> <small>12 Sales Today</small>--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li class="media event">--}}
                                        {{--<a class="pull-left border-aero profile_thumb">--}}
                                            {{--<i class="fa fa-user aero"></i>--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<a class="title" href="#">Ms. Mary Jane</a>--}}
                                            {{--<p><strong>$2300. </strong> Agent Avarage Sales </p>--}}
                                            {{--<p> <small>12 Sales Today</small>--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li class="media event">--}}
                                        {{--<a class="pull-left border-green profile_thumb">--}}
                                            {{--<i class="fa fa-user green"></i>--}}
                                        {{--</a>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<a class="title" href="#">Ms. Mary Jane</a>--}}
                                            {{--<p><strong>$2300. </strong> Agent Avarage Sales </p>--}}
                                            {{--<p> <small>12 Sales Today</small>--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}



        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Weekly Summary <small>Activity shares</small></h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}

                        {{--<div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">--}}
                            {{--<div class="col-md-7" style="overflow:hidden;">--}}
                        {{--<span class="sparkline_one" style="height: 160px; padding: 10px 25px;">--}}
                                      {{--<canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>--}}
                                  {{--</span>--}}
                                {{--<h4 style="margin:18px">Weekly sales progress</h4>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-5">--}}
                                {{--<div class="row" style="text-align: center;">--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<canvas id="canvas1i" height="110" width="110" style="margin: 5px 10px 10px 0"></canvas>--}}
                                        {{--<h4 style="margin:0">Bounce Rates</h4>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<canvas id="canvas1i2" height="110" width="110" style="margin: 5px 10px 10px 0"></canvas>--}}
                                        {{--<h4 style="margin:0">New Traffic</h4>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<canvas id="canvas1i3" height="110" width="110" style="margin: 5px 10px 10px 0"></canvas>--}}
                                        {{--<h4 style="margin:0">Device Share</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}



        {{--<div class="row">--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Top Profiles <small>Sessions</small></h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item One Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Three Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-4">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Top Profiles <small>Sessions</small></h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item One Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Three Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-4">--}}
                {{--<div class="x_panel">--}}
                    {{--<div class="x_title">--}}
                        {{--<h2>Top Profiles <small>Sessions</small></h2>--}}
                        {{--<ul class="nav navbar-right panel_toolbox">--}}
                            {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="dropdown">--}}
                                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Settings 1</a>--}}
                                    {{--</li>--}}
                                    {{--<li><a href="#">Settings 2</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="x_content">--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item One Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Two Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                        {{--<article class="media event">--}}
                            {{--<a class="pull-left date">--}}
                                {{--<p class="month">April</p>--}}
                                {{--<p class="day">23</p>--}}
                            {{--</a>--}}
                            {{--<div class="media-body">--}}
                                {{--<a class="title" href="#">Item Three Title</a>--}}
                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>--}}
                            {{--</div>--}}
                        {{--</article>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@stop