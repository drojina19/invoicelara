@extends('admin.baselayout.baselayout')
@section('main-content')

    {{--coded by sagar date:wed aug9--}}
    <div class="page-title">
        <div class="pull-left">
            <h3>Change Password</h3>
        </div>

        {{--<div class="pull-right">--}}
            {{--<a href="{{url('payamount/'.$invoice->id)}}" class="btn btn-primary pull-right">List {{$invoice->invoice_no}} Payments</a>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('psd'))
        <div class="alert alert-danger" id="status">
            {{Session::get('psd')}}
        </div>
    @endif
    <form id="demo-form2" class="form-horizontal form-label-left" action="{{route('password.update')}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Change Password</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Old Password *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="password" class="form-control" data-inputmask="'mask': '99/99/9999'"name="old_password" id="old_password" value="{{old('old_password')}}" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">New Password *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="password" class="form-control" data-inputmask="'mask': '99/99/9999'"name="new_password" id="new_password" value="{{old('new_password')}}" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Confirm Password *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="password" class="form-control" data-inputmask="'mask': '99/99/9999'"name="confirm_password"  id="confirm_password" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Show Password</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="checkbox"  id="show_password">
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-default" >Reset</button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
@section('custom-scripts')
    <script>
        $('#show_password').on('change', function () {
            $(".x_content input").each(function() {
                if($(this).attr('type')=="password"){
                    $(this).attr('type','text');
                }else if($(this).attr('type')=="text"){
                    $(this).attr('type','password');
                }
            });
        });
    </script>
@endsection
@stop