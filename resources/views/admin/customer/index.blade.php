@extends('admin.baselayout.baselayout')
@section('main-content')
 {{-- coded by sagar kc
    * date: wed aug 9 2017--}}



    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>List Customers</h3>
            </div>

            <div class="pull-right ">
                <a href="{{route('customer.create')}}" class="btn btn-primary pull-right">Add Customer</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        @if(\Illuminate\Support\Facades\Session::has('customer'))
            <div class="alert alert-success text-center col-md-12" id="status">
                {{\Illuminate\Support\Facades\Session::get('customer')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Customers</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-customer" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th style="width:35%;">Address</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>Status</th>
                                <th>Action(s)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sn=1; ?>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$sn}}</td>
                                    <td>{{$customer->name}}</td>
                                    <td>
                                        {{$customer->street_address}} {{$customer->suburb}} {{$customer->state}}, {{$customer->zipcode}}, {{$customer->country}}
                                    </td>
                                    <td>{{$customer->email}}</td>
                                    <td>
                                        {{$customer->phone1}}
                                        @if($customer->phone2!=null)
                                            ,<br>{{$customer->phone2}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($customer->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td style="width: 8%">
                                        <a href="{{route('customer.show',['id'=>$customer->id])}}" class="btn btn-xs btn-default pull-left" title="View Detail"><i class="fa fa-search-plus"></i></a>
                                        <a href="{{route('customer.edit',['id'=>$customer->id])}}" class="btn btn-xs btn-info pull-left" title="Edit"><i class="fa fa-edit"></i></a>
                                        @if(count($customer->invoices)==0)
                                        <form action={{url('customer/'.$customer->id)}} method="POST" class="pull-left">

                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('{{\App\Constant\Constant::DELETE_SOFT}}')"><i class="fa fa-trash-o" title="Delete"></i></button>

                                        </form>
                                        @endif()
                                        <div class="clearfix"></div>
                                    </td>
                                </tr>
                                <?php $sn++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@section('custom-scripts')
    <script>
        $("#datatable-customer").dataTable({});

    </script>
@endsection


@stop