@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="page-title col-md-12">
            <div class="pull-left">
                <h3>{{ucfirst($customer->name)}}</h3>
            </div>

            <div class="pull-right">
                <a href="{{route('customer.index')}}" class="btn btn-primary pull-right">List Customers</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>



        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Customer's Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Customer Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->name}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ABN:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->abn}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> ATTN:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->attn}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Status:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                @if($customer->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Inactive</span>
                                @endif
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Contact Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Email:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->email}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 1:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->phone1}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 2:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$customer->phone2==null?'-':$customer->phone2}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Address</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="">Street Address: </label>
                        {{$customer->street_address}}
                    </div>

                    <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="state">Suburb: </label>
                        {{$customer->suburb}}

                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="state">State: </label>
                        {{$customer->state}}

                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="">Zip Code: </label>
                        {{$customer->zipcode}}
                    </div>

                    <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                        <label for="country">Country:</label>
                        {{$customer->country}}

                    </div>
                </div>
            </div>

            {{-- <div class="col-md-12 col-sm-9 col-xs-12 " align="center">

                <a href="" class="btn btn-default">View Invoices</a>

            </div> --}}

        </div>

    </div>
@stop