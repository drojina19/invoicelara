@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="pull-left">
            <h3>Edit Customer</h3>
        </div>

        <div class="pull-right">
            <a href="{{route('customer.index')}}" class="btn btn-primary pull-right">List Customers</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form id="demo-form2" class="form-horizontal form-label-left"
          action="{{route('customer.update',['id'=>$customer->id])}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="patch">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Customers Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Customer Name *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="name"
                                       data-inputmask="'mask': '99/99/9999'" value="{{$customer->name}}">
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ATTN *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="attn"
                                       data-inputmask="'mask': '99/99/9999'" value="{{$customer->attn}}" required>
                                <span class="fa fa-paper-plane form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ABN *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="abn" data-inputmask="'mask': '99/99/9999'"
                                       value="{{$customer->abn}}" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contact Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Email *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="email" class="form-control" name="email"
                                       data-inputmask="'mask': '99/99/9999'" value="{{$customer->email}}" required>
                                <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 1 *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone1"
                                       data-inputmask="'mask': '99/99/9999'" value="{{$customer->phone1}}" required>
                                <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 2</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone2"
                                       data-inputmask="'mask': '99/99/9999'" value="{{$customer->phone2}}" >
                                <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Address</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>

                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Street Address *</label>
                            <input type="text" class="form-control" name="street_address" id="inputSuccess3"
                                   value="{{$customer->street_address}}" required>
                            <span class="fa fa-map-marker form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Suburb *</label>
                            <input type="text" class="form-control" name="suburb" id="inputSuccess3"
                                    value="{{$customer->suburb}}" required>
                            <span class="fa fa-location-arrow form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="state">State *</label>
                            <select class="form-control" name="state" required>
                                <option value="NSW" {{$customer->state=="NSW"?'selected':''}}>NSW</option>
                                <option value="VIC" {{$customer->state=="VIC"?'selected':''}}>VIC</option>
                                <option value="QLD" {{$customer->state=="QLD"?'selected':''}}>QLD</option>
                                <option value="ACT" {{$customer->state=="ACT"?'selected':''}}>ACT</option>
                                <option value="SA" {{$customer->state=="SA"?'selected':''}}>SA</option>
                                <option value="NT" {{$customer->state=="NT"?'selected':''}}>NT</option>
                                <option value="WA" {{$customer->state=="WA"?'selected':''}}>WA</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Zip Code *</label>
                            <input type="text" class="form-control" id="inputSuccess5" name="zipcode" value="{{$customer->zipcode}}" required>
                            <span class="fa fa-file-archive-o form-control-feedback right" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="country">Country *</label>
                            <select class="form-control" name="country" required>
                                <option value="Australia" {{$customer->country=='Australia'?'selected':''}}>Australia</option>
                                <option value="Nepal" {{$customer->country=='Nepal'?'selected':''}}>Nepal</option>
                            </select>
                        </div>


                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Status *</label>
                            <select class="form-control" name="status" required>
                                <option value="1"{{$customer->status==1?'selected':''}}>Active</option>
                                <option value="0" {{$customer->status==0?'selected':''}}>Inactive</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <a href="{{route('customer.index')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </form>

@stop