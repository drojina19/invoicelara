
@extends('admin.baselayout.baselayout')
@section('main-content')

    {{--coded by sagar date:wed aug9--}}
    <div class="page-title">
        <div class="pull-left">
            <h3>Add Customer</h3>
        </div>

        <div class="pull-right">
            <a href="{{route('customer.index')}}" class="btn btn-primary pull-right">List Customers</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form id="demo-form2" class="form-horizontal form-label-left" action="{{route('customer.store')}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Customers Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Customer Name *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="name" data-inputmask="'mask': '99/99/9999'" value="{{old('name')}}" required>
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ATTN *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="attn" data-inputmask="'mask': '99/99/9999'" value="{{old('attn')}}" required>
                                <span class="fa fa-paper-plane form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ABN *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="abn" data-inputmask="'mask': '99/99/9999'" value="{{old('abn')}}" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contact Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Email *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="email" class="form-control" name="email" data-inputmask="'mask': '99/99/9999'" value="{{old('email')}}" required>
                                <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 1 *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone1" data-inputmask="'mask': '99/99/9999'" value="{{old('phone1')}}" required >
                                <span class="fa fa-phone  form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 2</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone2" data-inputmask="'mask': '99/99/9999'" value="{{old('phone2')}}">
                                <span class="fa fa-phone  form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Address</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Street Address *</label>
                            <input type="text" class="form-control" name="street_address" id="inputSuccess3" value="{{old('street_address')}}" required>
                            <span class="fa fa-map-marker form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Suburb *</label>
                            <input type="text" class="form-control" name="suburb" id="inputSuccess3" value="{{old('suburb')}}" required>
                            <span class="fa fa-location-arrow form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">State *</label>
                            <select name="state" class="form-control" required>
                                <option value="NSW" @if(old('state') == 'NSW')selected @endif>NSW</option>
                                <option value="VIC" @if(old('state') == 'VIC')selected @endif>VIC</option>
                                <option value="QLD" @if(old('state') == 'QLD')selected @endif>QLD</option>
                                <option value="ACT" @if(old('state') == 'ACT')selected @endif>ACT</option>
                                <option value="SA" @if(old('state') == 'SA')selected @endif>SA</option>
                                <option value="NT" @if(old('state') == 'NT')selected @endif>NT</option>
                                <option value="WA" @if(old('state') == 'WA')selected @endif>WA</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Zip Code *</label>
                            <input type="text" class="form-control" name="zipcode" id="inputSuccess5" value="{{old('zipcode')}}" required>
                            <span class="fa fa-file-archive-o form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Country *</label>
                            <select class="form-control" name="country" value="{{old('country')}}" required>
                                <option value="Australia" @if(old('country') == 'Australia')selected @endif>Australia</option>
                                <option value="Nepal" @if(old('country')=='Nepal')selected @endif>Nepal</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Status *</label>
                            <select class="form-control" name="status" value="{{old('street_address')}}" required>
                                <option value="1" @if(old('status')=='1')selected @endif>Active</option>
                                <option value="0" @if(old('status')=='0')selected @endif>Inactive</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <a href="{{route('customer.index')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop