@extends('admin.baselayout.baselayout')
@section('main-content')
    {{-- coded by sagar kc
       * date: wed aug 9 2017--}}



    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>List Deleted Customers</h3>
            </div>

            <div class="pull-right ">
                <a href="{{route('customer.index')}}" class="btn btn-primary pull-right">List Customers</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        @if(\Illuminate\Support\Facades\Session::has('customer'))
            <div class="alert alert-success text-center col-md-12" id="status">
                {{\Illuminate\Support\Facades\Session::get('customer')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Customers</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th style="width:35%;">Address</th>
                                <th>Email</th>
                                <th>Contact Number</th>

                                <th>Action(s)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>
                                        {{$customer->street_address}}, {{$customer->suburb}} {{$customer->state}}, {{$customer->zipcode}}, {{$customer->country}}
                                    </td>
                                    <td>{{$customer->email}}</td>
                                    <td>
                                        {{$customer->phone1}}
                                        @if($customer->phone2!=null)
                                            ,<br>{{$customer->phone2}}
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{url('restorecustomer/'.$customer->id)}}" class="btn btn-xs btn-default" title="Restore Customer" onclick="return confirm('{{\App\Constant\Constant::RESTORE_ITEM}}')"><i class="fa fa-undo"></i></a>
                                        <a href="{{url('forcedeletecustomer/'.$customer->id)}}" class="btn btn-xs btn-danger" title="Delete Permanently" onclick="return confirm('{{\App\Constant\Constant::DELETE_PERMANENT}}')">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



@stop