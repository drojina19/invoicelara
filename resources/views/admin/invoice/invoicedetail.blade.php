@extends('admin.baselayout.baselayout')
<style>
    ul.supplier>li
    {
        list-style-type: none;
        line-height: 1.7;
    }
</style>

@section('main-content')
    <div class="page-title">
        <div class="col-md-11">
            <div class="pull-left">
                <h3>TAX INVOICE</h3>
            </div>

            <div class="pull-right">
                <a href="{{url('/invoice-sendmail/'.$invoice->id)}}" class="btn btn-info" title="Mail To Cutomer"><i class="fa fa-envelope"></i>&nbsp; Mail</a>
                <a href="javascript:void(0)" data-url="{{url("print-invoice/".$invoice->id)}}" class="printInvoice btn btn-primary" title="Print"><i class="fa fa-print"></i>&nbsp;Print</a>
                <a href="{{url('/downloadpdf/'.$invoice->id)}}" class="btn btn-info" title="Download Pdf"><i class="fa fa-file-pdf-o"></i>&nbsp; Download Pdf</a>

                <a href="{{url('invoice')}}" class="btn btn-success" title="Invoice List"><i class="fa fa-list"></i>&nbsp;Invoice List</a>
                <a href="{{url()->previous()}}" class="btn btn-danger" title="Invoice List"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>



        <div class="col-md-11 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Invoice Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="row">
                        <div class="container" style="width: 100%; margin-bottom: 30px">
                            <div class="col-md-12">
                                <!-- <div class="container" style="width: 100%"> -->

                                <div class="col-md-3">
                                    <img class="pull-right" src="{{asset('images/supplier/'.$invoice->supplier->logo)}}"  alt="{{config('app.name')}}" style="width:100%">
                                </div>
                                <div class="col-md-9" align="right">
                                    <ul class="supplier">
                                        <li><b style="font-size: 1.5em">{{$invoice->supplier->supplier_name}}</b></li>
                                        <li>{{$invoice->supplier->street}}</li>
                                        <li>{{$invoice->supplier->city}},{{$invoice->supplier->state}},{{$invoice->supplier->zip_code}},{{$invoice->supplier->country}}<br></li>
                                        <li>{{$invoice->supplier->website}}</li>
                                        <li>E: {{$invoice->supplier->email}}</li>
                                        <li>Contact: {{$invoice->supplier->phone1}},{{$invoice->supplier->phone2}}</li>
                                        <li>ABN: {{$invoice->supplier->abn}}</li>
                                    </ul>

                                </div>

                                <div class="clearfix"></div>

                            </div>
                            <div class="clearfix"></div>
                            <div class=" col-md-12 mydivider"></div>
                            <div class="clearfix"></div>
                            <div class="col-md-12"> <hr style="border: 2px solid #000;"></div>

                            <div class="col-md-12">
                                <div class="container" style="width: 100%">
                                    <label class="" style="font-size: 1.3em;">Tax Invoice: {{$invoice->invoice_no}} </label> &nbsp;<b>(Please use this reference number for payment)</b>
                                </div>
                            </div>

                            <div class="col-md-12" style="margin-top: 20px">

                                <div class="col-md-12">
                                    <label style="font-size: 1.2em;color:rgba(114,141,212,1)">
                                        Invoice Date: {{$invoice->issue_date}}
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Due Date: {{$invoice->due_date}}
                                    </label>
                                </div>


                                {{--<div class="col-md-4">--}}
                                        {{--<label style="font-size: 1.2em; color: white; background-color:green">{{$invoice->status}}</label>--}}
                                    {{--</div>--}}
                                <div class="clearfix"></div>


                            </div>
                            <div class="col-md-12 textc" style="margin-top: 5px;background-color: rgba(219,229,241,1)">
                                <div class="col-md-9 ">
                                    <h2><b><u>Invoice To:</u></b></h2>

                                    <label><b>{{$invoice->customer->name}}</b></label><br>
                                    <label>ABN:</label> {{$invoice->customer->abn}}<br>
                                    <label>Address:</label> {{$invoice->customer->treet_address}} , {{$invoice->customer->suburb}},{{$invoice->customer->zipcode}},{{$invoice->customer->country}}<br>
                                    <label>ATTN:</label> {{$invoice->customer->attn}}<br>
                                    <label>Contact No:</label> {{$invoice->customer->phone1}}{{$invoice->customer->phone2=!null?", ".$invoice->customer->phone2:""}}<br>
                                    <label>Email:</label> {{$invoice->customer->email}}
                                </div>
                                <div class="col-md-3" align="right">
                                    <h3>Balance Due:</h3>
                                    <b style="font-size: 1.8em;color:rgba(0,128,0,1)">${{number_format($invoice->amt_owed,2,'.',',')}}</b>
                                    <br>
                                    <br>
                                    @if($invoice->status==0)
                                        @if(\Carbon\Carbon::parse($invoice->due_date)->format("Y-m-d")<date('Y-m-d'))
                                            <span title='Overdue Invoice' class='label label-warning' style="font-size: 1.7em;">Overdue</span>
                                        @endif
                                    @else
                                        <span class='label label-success' style="font-size: 1.7em;">Paid</span>
                                    @endif

                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12" style="padding:0;margin-top: 3px">
                                <div class="" style="width: 100%">
                                    <table class="table" border="1"style="border-collapse: collapse">
                                        <thead>
                                        <tr>
                                            <th style="width:2%">No</th>
                                            <th style="width: 40%">Item Description</th>
                                            <th style="width: 10%; text-align: center" >Qty</th>
                                            <th style="width: 10%; text-align: center">Price(AUD)</th>
                                            <th style="width: 10%; text-align: right">Amount(AUD)</th>
                                        </tr>
                                        </thead>
                                        <tbody id="inv-detail">
                                        <?php $a = 1 ?>
                                        @foreach($invoice->invoicedetails as $invdetail)

                                            <tr>
                                                <td><?= $a ?></td>
                                                <td>{{$invdetail->product->name}}</td>
                                                <td style="text-align: center">{{$invdetail->quantity}}</td>
                                                <td style="text-align: center">${{number_format($invdetail->rate,2,'.',',')}}</td>
                                                <td style="text-align: right">${{number_format($invdetail->amount,2,'.',',')}}</td>
                                            </tr>



                                            <?php $a = 1 + $a ?>

                                        @endforeach

                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="4"><font class="pull-right"><b>Sub Total:</b></font></td>
                                            <td style="text-align: right">${{number_format($invoice->total,2,'.',',')}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><font class="pull-right"><b style="paddding-right: 10px">{{$invoice->gst_type=="GTS10"?"GST - 10%":"GST Including"}}</b></font></td>
                                            <td style="text-align: right">${{number_format($invoice->gst,2,'.',',')}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><font class="pull-right"><b style="paddding-right: 10px; color: red">Grand Total:</b></font></td>
                                            <td style="text-align: right"><b style="color: red">${{number_format($invoice->grandtotal,2,'.',',')}}</b></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12" align="center">
                                <b style="font-size: 1.1em; font-weight: 800;">
                                    <h4 style="background-color:#006793;color:white;padding-top: 5px;padding-bottom: 5px;">How To Pay</h4>

                                    Banking Details:
                                   <br>
                                    Name: {{$invoice->supplier->supplier_name}}
                                    <br>
                                    BSB: {{$invoice->payment->bsb}}
                                    &nbsp;
                                    Ac/No:{{$invoice->payment->account_no}}
                                    <br>
                                    {{$invoice->payment->bank_name}}
                                    <br>
                                    {{$invoice->payment->pay_method}}


                                </b>
                            </div>
                            <div class="col-md-12" style="background-color:black;color:white;padding:0;padding-bottom: 5px;padding-top: 5px;">
                                <div class="col-md-2" style="padding:0 0 0 5px; font-size:1.2em;">
                                    Genius IT Solutions
                                </div>
                                <div class="col-md-10">
                                    <span style="font-size:1.2em;">HARDWARE | SOFTWARE | SERVER | WEBSITE | IT SUPPORT | GRAPHIC DESIGN | APP DEVELOPMENT</span>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @section('custom-scripts')
        <script src="{{asset('js/custom.js')}}"></script>
    @endsection
@stop