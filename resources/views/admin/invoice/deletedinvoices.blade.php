@extends('admin.baselayout.baselayout')
@section('main-content')

    {{--coded by:rojina
     date:9th August--}}
    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>Deleted Invoice</h3>
            </div>

            <div class="pull-right">
                <a href="{{route('invoice.create')}}" class="btn btn-primary pull-right">Add invoice</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                @if(\Illuminate\Support\Facades\Session::has('invoice'))
                    <div class="alert alert-success text-center" id="status">
                        {{\Illuminate\Support\Facades\Session::get('invoice')}}
                    </div>
                @endif
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Deleted Invoices</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Invoice No.</th>
                                <th>Supplier</th>

                                <th>Customer</th>
                                <th>Issue Date</th>
                                <th>Due Date</th>
                                <th> Grand Total</th>
                                <th>Amount Due</th>
                                <th>Amount Paid</th>
                                {{--<th>Paid/Unpaid</th>--}}
                                <th> Action(s)</th>
                                {{--<th>Amount</th>--}}
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($deletedinvoices as $invoice)
                                <tr>

                                    <td>{{$invoice->invoice_no}}</td>
                                    <td>{{$invoice->supplier->supplier_name}}</td>

                                    <td>{{$invoice->customer->name}}</td>
                                    <td>{{\Carbon\Carbon::parse($invoice->issue_date)->format('d-m-Y')}} </td>
                                    <td>{{\Carbon\Carbon::parse($invoice->due_date)->format('d-m-Y')}}</td>
                                    <td>${{$invoice->grandtotal}}</td>
                                    <td>${{$invoice->amt_owed}}</td>
                                    <td>${{(float)$invoice->grandtotal-(float)$invoice->amt_owed}}</td>

                                    {{--<td>--}}
                                        {{--@if($invoice->status==0)--}}
                                            {{--<span class='label label-danger'>Unpaid</span>--}}
                                            {{--@if($invoice->due_date>date('Y-m-d'))--}}
                                                {{--&nbsp;&nbsp;&nbsp;<span title='Overdue Invoice' class='label label-warning'>D</span>--}}
                                            {{--@endif--}}
                                        {{--@else--}}
                                            {{--<span class='label label-success'>Paid</span>--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                    <td>

                                        <a href="{{url('/deletedinvoicesinformation/'.$invoice->id)}}" class="btn btn-default btn-xs"><i class="fa fa-search-plus" title="View Detail"></i></a>
                                        <a href="{{url('/restoredeletedinvoice/'.$invoice->id)}}"  class="btn btn-xs btn-default" title="Restore Deleted Invoice" onclick="return confirm('{{\App\Constant\Constant::RESTORE_ITEM}}')"> <i class="fa fa-undo"> </i></a>
                                        <a href="{{url('/forcedeletedinvoice/'.$invoice->id)}}" title="Delete Permanently" class="btn btn-xs btn-danger" onclick="return confirm('{{\App\Constant\Constant::DELETE_PERMANENT}}')"> <i class="fa fa-trash-o"> </i></a>

                                    </td>
                                    <td></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop




