<style type="text/css">
    ul.biller{
        padding-left: 0px;
    }
    ul.biller li{
        list-style: none;
        line-height: 25px;

    }
    .table, .table1 {
        width: 650px;
        margin-bottom: 5px;
        margin-left: 30px;
    }
    .table > thead > tr > th,
    .table > tbody > tr > th,
    .table > tfoot > tr > th,
    .table > thead > tr > td,
    .table > tbody > tr > td,
    .table > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .table1 > thead > tr > th,
    .table1 > tbody > tr > th,
    .table1 > tfoot > tr > th,
    .table1 > thead > tr > td,
    .table1 > tbody > tr > td,
    .table1 > tfoot > tr > td {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        /*border-top: 1px solid #ddd;*/
    }
    .table > thead > tr > th {
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
        text-align: left;
    }

    .table > tbody + tbody {
        border-top: 2px solid #ddd;
    }
    .table .table {
        background-color: #fff;
    }
    .pull-right{
        float:right;
        /*padding-left: 470px;*/
    }
</style>
<body>

<table class="table1" width="650px">
    <tr>
        <td>
            <img class="" src="{{asset('images/supplier/'.$invoice->supplier->logo)}}" style="margin-top:25px;width: 225px; height:110px;">
        </td>
        <td colspan="2" align="right">

            <b style="font-size: 20px">{{$invoice->supplier->supplier_name}}</b><br>
            {{$invoice->supplier->street}}<br>
            {{$invoice->supplier->city}},{{$invoice->supplier->state}},{{$invoice->supplier->zip_code}},{{$invoice->supplier->country}}<br>
            {{$invoice->supplier->website}}<br>
            E: {{$invoice->supplier->email}}<br>
            Contact: {{$invoice->supplier->phone1}},{{$invoice->supplier->phone2}}<br>
            ABN: {{$invoice->supplier->abn}}<br>
        </td>
    </tr>
</table>
<table class="table1" width="650px">
    <tr>
        <td>
            <hr style="height: 7px;
                                    margin: 9px 1px;
                                    overflow: hidden;
                                    background: black;
                                    border-bottom: 1px solid black;
                                    -webkit-print-color-adjust: exact;">
        </td>
    </tr>
    <tr>
        <td>
            <label class="" style="font-size: 1.3em;">Tax Invoice: {{$invoice->invoice_no}}</label>  &nbsp;<b>(Please use this reference number for payment)</b>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <?php date_default_timezone_set('Australia/Sydney'); ?>
            <label style="font-size: 1.2em;color:rgba(114,141,212,1)">
                Invoice Date: {{\Carbon\Carbon::parse($invoice->issue_date)->format("d-m-Y")}}
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Due Date: {{\Carbon\Carbon::parse($invoice->due_date)->format("d-m-Y")}}
            </label>
        </td>
    </tr>
</table>
<table class="table1" width="650px" style="background: rgba(219,229,241,1);color: rgba(47,46,107,1); -webkit-print-color-adjust: exact;">
    <tr >
        <td>
            <b style="padding-bottom: 60px;padding-top:60px">Invoice To:</b><br>

            <label for=""><b>{{$invoice->customer->name}}</b></label><br>
            {{$invoice->customer->abn}}<br>
            <label>Address:</label> {{$invoice->customer->treet_address}} , {{$invoice->customer->suburb}},{{$invoice->customer->zipcode}},{{$invoice->customer->country}}<br>
            <label>ATTN:</label> {{$invoice->customer->attn}}<br>
            <label>Contact No: </label>{{$invoice->customer->phone1}}{{$invoice->customer->phone2=!null?", ".$invoice->customer->phone2:""}}<br>
            <label>Email: </label>{{$invoice->customer->email}}
        </td>
        <td align="right">
            <h4 style="margin-top: 2px;margin-bottom: 2px;">Balance Due:</h4>
            <b style="font-size: 1.8em; color:green;">${{number_format($invoice->amt_owed,2,'.',',')}}</b>
            <br>
            <br>
            @if($invoice->status==0)
                @if(\Carbon\Carbon::parse($invoice->due_date)->format("Y-m-d")<date('Y-m-d'))
                    <span style="background:#d9534f; color:#fff; font-size: 30px; font-weight: 800; border: 4px solid #d9534f;padding:4px 10px;border-radius: 10px">Overdue</span>
                @endif
            @else
                <span style="background:#17743a; color:#fff; font-size: 30px; font-weight: 800; border: 4px solid #17743a;padding:4px 10px;border-radius: 10px">Paid</span>
            @endif
        </td>
    </tr>
</table>
<table class="table" border="1"style="border-collapse: collapse" width="650px">
    <thead>
    <tr>
        <td style="width: 25px"><b>No</b></td>
        <td><b>Item Description</b></td>
        <td style="width: 70px"><b>Qty</b></td>
        <td style="width: 70px"><b>Price(AUD)</b></td>
        <td style="width: 100px"><b>Amount(AUD)</b></td>
    </tr>
    </thead>
    <tbody id="inv-detail">

    <?php
    $sn = 1;
    ?>
    @foreach($invoice->invoicedetails as $invdetail)
    <tr>
        <td>
            {{$sn}}
            <?php $sn++; ?>
        </td>
        <td>{{$invdetail->product->name}}</td>
        <td style="text-align: center">{{$invdetail->quantity}}</td>
        <td style="text-align: center">${{number_format($invdetail->rate,2,'.',',')}}</td>
        <td style="text-align: right">${{number_format($invdetail->amount,2,'.',',')}}</td>
    </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <td colspan="4"><font class="pull-right"><b>Sub Total:</b></font></td>
        <td style="text-align: right">${{number_format($invoice->total,2,'.',',')}}</td>
    </tr>
    <tr>
        <td colspan="4">
            <font class="pull-right">
                <b style="paddding-right: 10px">
                    {{$invoice->gst_type==\App\Constant\Constant::GST10?\App\Constant\Constant::GST_10:App\Constant\Constant::GST_INCLUDED}}: 
                </b>
            </font>
        </td>
        <td style="text-align: right">${{number_format($invoice->gst,2,'.',',')}}</td>
    </tr>
    <tr>
        <td colspan="4"><font class="pull-right"><b style="paddding-right: 10px; color: red">Grand Total:</b></font></td>
        <td style="text-align: right"><b style="color: red">${{number_format($invoice->grandtotal,2,'.',',')}}</b></td>
    </tr>
    </tfoot>
</table>

<table class="table1" width="650px">
    <tr>
        <td align="center">
            <b style="font-size: 1.1em; font-weight: 800;">
                <h4 style="background-color:#006793;color:white;padding-top: 5px;padding-bottom: 5px;-webkit-print-color-adjust: exact;">How To Pay</h4>

                Banking Details:
                <br>
                Name: {{$invoice->supplier->supplier_name}}
                <br>
                BSB: {{$invoice->payment->bsb}}
                &nbsp;
                Ac/No:{{$invoice->payment->account_no}}
                <br>
                {{$invoice->payment->bank_name}}
                <br>
                {{$invoice->payment->pay_method}}


            </b>
        </td>
    </tr>
</table>
<table class="table1" width="650px" style="background:black; color: white; -webkit-print-color-adjust: exact;">
    <tr>
        <td style="font-size:15px">
            Genius It Solutions
        </td>
        <td style="font-size:9px; vertical-align: middle;">
            <span >HARDWARE | SOFTWARE | SERVER | WEBSITE | IT SUPPORT | GRAPHIC DESIGN | APP DEVELOPMENT</span>
        </td>
    </tr>
</table>
