@extends('admin.baselayout.baselayout')
@section('main-content')

    <div class="page-title">
        <div class="title_left">
            <h3>Add New Invoice</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form class="form-horizontal form-label-left" action="{{route('invoice.store')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">

                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Tax Invoice:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="hidden"  name="invoice_no" id="invoice_no" value="{{$invoiceno}}">
                                {{$invoiceno}}

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Supplier Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <select name="supplier_id" id="supplier_id" class="form-control" required>
                                    <option value="" selected disabled>Choose Supplier</option>
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->supplier_name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Invoice To:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <select name="customer_id" id="customer_id" class="form-control" required>
                                    <option value="" selected disabled>Choose Customer</option>
                                    @foreach($customers as $customer)
                                        <option value="{{$customer->id}}">{{$customer->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Issue Date:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text"  id="issue_date" class="form-control" name="issue_date" data-inputmask="'mask': '99/99/9999'" required>
                                <span class="fa fa-calendar-check-o form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Due Date:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text"  id="due_date" class="form-control" name="due_date" data-inputmask="'mask': '99/99/9999'" required>
                                <span class="fa fa-calendar-check-o form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-11 col-sm-11 col-xs-11">

                <div class="x_panel">
                    <div class="x_content">

                        <table class="table table-bordered">

                            <thead>
                            <tr>
                                <td>No</td>
                                <td>S.N</td>
                                <td style="width:500px;text-align: center">Item Description</td>
                                <td style="text-align: center"> Quantity</td>
                                <td style="text-align: center"> Rate(AUD)</td>
                                <td style="text-align: right">Amount(AUD)</td>



                            </tr>
                            </thead>

                            <tbody id="inv-detail">
                            <tr >

                                <td></td>
                                <td class="inc1">1</td>
                                <td >
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <select name="product_id[]"  class="form-control product_name">
                                                <option value="" selected disabled>Choose Product</option>
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                            <textarea rows="3" class="col-md-12 form-control" name="description[]" id="description" placeholder="Product Description"></textarea>


                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-center quantity" name="quantity[]" data-inputmask="'mask': '99/99/9999'" disabled required>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-center rate" name="rate[]" data-inputmask="'mask': '99/99/9999'" disabled required>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-right amount" data-inputmask="'mask': '99/99/9999'" disabled >
                                    <input type="hidden" class="form-control amount1" name="amount[]" data-inputmask="'mask': '99/99/9999'" required>
                                </td>
                            </tr>
                            <tr class="product-hidden hidden" id="product">
                                <td ><a href="javascript:void(0)" onclick="removeRow(this)"><i class="fa fa-trash-o" ></i></a></td>
                                <td class="increase"></td>
                                <td>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <select name="product_id[]" class="form-control product_name" disabled>
                                                <option value="" selected disabled>Choose Product</option>
                                                @foreach($products as $product)
                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                            <textarea rows="3" class="col-md-12 form-control" name="description[]" id="description" placeholder="Product Description" disabled></textarea>

                                        </div>
                                    </div>
                                </td>
                                <td >
                                    <input type="text" class="form-control text-center quantity" name="quantity[]" data-inputmask="'mask': '99/99/9999'" disabled required>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-center rate" name="rate[]" data-inputmask="'mask': '99/99/9999'" disabled required>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-right amount" data-inputmask="'mask': '99/99/9999'" disabled>
                                    <input type="hidden" class="form-control amount1" name="amount[]" data-inputmask="'mask': '99/99/9999'" disabled required>
                                </td>
                            </tr>

                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5" style="text-align: right;font-weight:700; vertical-align:middle">Sub Total</td>
                                <td>
                                    <input type="text" class="form-control text-right subtotal" data-inputmask="'mask': '99/99/9999'" disabled>
                                    <input type="hidden" class="form-control subtotal1" name="total"  data-inputmask="'mask': '99/99/9999'" required>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align:right;font-weight:700; vertical-align:middle">
                                    <div class="col-md-3 pull-right">
                                        <select id="gst_type" name="gst_type" class="form-control" onchange="gstChanged(this.value)">
                                            <option value="{{\App\Constant\Constant::GST10}}">{{\App\Constant\Constant::GST_10}}</option>
                                            <option value="{{\App\Constant\Constant::GSTINCLUDED}}">{{\App\Constant\Constant::GST_INCLUDED}}</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control text-right gst" data-inputmask="'mask': '99/99/9999'" disabled>
                                    <input type="hidden" class="form-control gst1" name="gst" data-inputmask="'mask': '99/99/9999'" required>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: right; color: red; font-weight:800; vertical-align:middle;" >Grand Total</td>
                                <td>
                                    <input type="text" class="form-control text-right grandtotal"  data-inputmask="'mask': '99/99/9999'" disabled>
                                    <input type="hidden" class="form-control  grandtotal1" name="grandtotal" data-inputmask="'mask': '99/99/9999'" required>
                                </td>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <button type="button" class="btn btn-success" style="float:right" id="adding_row">Add New Row</button>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-11 col-sm-11 col-xs-11">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>

                            <a href="{{route('invoice.index')}}" class="btn btn-danger">Cancel</a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>

@section('custom-script')
    <script src="{{asset('js/custom.js')}}"></script>
@stop


@stop