@extends('admin.baselayout.baselayout')
@section('main-content')
    {{--coded by:rojina
     date:9th August--}}
    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>Invoice</h3>
            </div>

            <div class="pull-right">
                <a href="{{route('invoice.create')}}" class="btn btn-primary pull-right">Add invoice</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                @if(\Illuminate\Support\Facades\Session::has('invoice'))
                    <div class="alert alert-success text-center" id="status">
                        {{\Illuminate\Support\Facades\Session::get('invoice')}}
                    </div>
                @endif
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Invoices</h2>
                        <div class="pull-right">
                            <select id="dropdown1" class="form-control">
                                <option value="">All Invoices</option>
                                <option value="All Paid">Paid</option>
                                <option value="Remaining Paid">Unpaid</option>
                                <option value="Over Due">Over Due</option>
                            </select>
                            <div class="clearfix"></div>

                        </div>
                        <div class="pull-right">
                            <select id="sel-cust" class="form-control">
                                <option value="" selected disabled>Choose Customer</option>
                                <option value="">All Customers</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->name}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                            <div class="clearfix"></div>
                            
                        </div>
                        <div class="pull-right">
                            <select id="sel-supplier" class="form-control">
                                <option value="" selected disabled>Choose Supplier</option>
                                <option value="">All Suppliers</option>
                                @foreach($suppliers as $supplier)
                                    <option value="{{$supplier->supplier_name}}">{{$supplier->supplier_name}}</option>
                                @endforeach
                            </select>
                            <div class="clearfix"></div>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-invoice" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Invoice No.</th>
                                <th>Supplier</th>
                                <th>Customer</th>
                                <th style="width: 8%">Issue Date</th>
                                <th> Grand Total</th>
                                <th>Amount Due</th>
                                <th>Amount Paid</th>
                                <th>Paid/<br>Unpaid</th>
                                <th style="width: 8.1%"> Action(s)</th>
                                <th>Amount</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($invoices as $invoice)
                                <tr>
                                    <td><b>{{$invoice->invoice_no}}</b></td>
                                    <td>{{$invoice->supplier->supplier_name}}</td>
                                    <td>{{$invoice->customer->name}}</td>
                                    <td>{{\Carbon\Carbon::parse($invoice->issue_date)->format('d-m-Y')}} </td>
                                    <td class="text-primary"><b>${{number_format($invoice->grandtotal,2,'.',',')}}</b></td>
                                    <td class="text-danger"><b>${{number_format($invoice->amt_owed,2,'.',',')}}</b></td>
                                    <td class="text-success"><b>${{number_format((float)$invoice->grandtotal-(float)$invoice->amt_owed, 2, '.', ',')}}</b></td>

                                    <td>
                                        @if($invoice->status==0)
                                            <span class='label label-danger'>Unpaid</span>
                                            <span class="hidden">Remaining Paid</span>

                                            @if(\Carbon\Carbon::parse($invoice->due_date)->format("Y-m-d")<date("Y-m-d"))
                                                &nbsp;&nbsp;&nbsp;<span title='Overdue Invoice' class='label label-warning'>D</span>
                                                <span class="hidden">Over Due</span>
                                            @endif
                                        @else
                                            <span class='label label-success'>Paid</span>
                                            <span class='hidden'>All Paid</span>
                                        @endif
                                    </td>
                                    <td>

                                        <a href="{{url('/invoice/'.$invoice->id)}}" class="btn btn-default btn-xs pull-left"><i class="fa fa-search-plus" title="View Detail"></i></a>
                                        <a href="{{url('invoice/'.$invoice->id.'/edit')}}" class="btn btn-info btn-xs pull-left" title="Edit"> <i class="fa fa-edit"></i></a>
                                        <form action={{url('invoice/'.$invoice->id)}} method="POST" class="pull-left">

                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('{{\App\Constant\Constant::DELETE_SOFT}}')"><i class="fa fa-trash-o" title="Delete"></i></button>

                                        </form>
                                        <div class="clearfix"></div>
                                        <a href="javascript:void(0)" data-url="{{url("print-invoice/".$invoice->id)}}" class="printInvoice btn btn-default btn-xs pull-left" title="Print Invoice"><i class="fa fa-print"></i></a>
                                        <a href="{{url('/downloadpdf/'.$invoice->id)}}"  class="btn btn-default btn-xs pull-left" title="Download PDF"><i class="fa fa-file-pdf-o"></i></a>
                                        <a href="{{url('/invoice-sendmail/'.$invoice->id)}}"  class="btn btn-default btn-xs pull-left" title="Send Mail To Customer"><i class="fa fa-envelope-o"></i></a>
                                        <div class="clearfix"></div>
                                    </td>
                                    <td >
                                        <a href="{{url('/payamount/'.$invoice->id)}}" title="View Paid Amount" class="btn btn-success btn-xs"><i class="fa fa-table"></i></a>
                                        @if($invoice->status==0)
                                            <a href="{{url('/addpayamount/'.$invoice->id)}}" title="Enter Paid Amount" class="btn btn-primary btn-xs"><i class="fa fa-dollar"></i> </a>
                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @section('custom-scripts')
        <script src="{{asset('js/custom.js')}}"></script>
    @endsection
@stop




