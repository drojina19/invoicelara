@extends('admin.baselayout.baselayout')
@section('custom-css')
    <style>
        #no-border-table>tbody>tr>td, #no-border-table>tbody>tr>th{
            border: none;
        }
    </style>
@endsection
@section('main-content')

    {{--coded by:rojina date:9th August--}}
    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>List All Payments for {{$invoice->invoice_no}}</h3>
            </div>

            <div class="pull-.right">
                @if($invoice->amt_owed>0)
                <a href="{{url('/addpayamount/'.$invoice->id)}}" class="btn btn-primary pull-right">Add Payamount</a>
                @endif
                <a href="{{url('/invoice/')}}" class="btn btn-success pull-right">List invoices</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                @if(\Illuminate\Support\Facades\Session::has('payamount'))
                    <div class="alert alert-success text-center" id="status">
                        {{\Illuminate\Support\Facades\Session::get('payamount')}}
                    </div>
                @endif
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Payments</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <table class="table" id="no-border-table">
                            <tr>
                                <th>Invoice No.:</th>
                                <td>{{$invoice->invoice_no}}</td>
                            </tr>
                            <tr>
                                <th>Supplier:</th>
                                <td>{{$invoice->supplier->supplier_name}}</td>
                            </tr>
                            <tr>
                                <th>Customer:</th>
                                <td>{{$invoice->customer->name}}</td>
                            </tr>
                            <tr>
                                <th>Remaining Amount Due:</th>
                                <td class="alert alert-danger"><span>{{$invoice->amt_owed}}</span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>

                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Action(s)</th>

                            </tr>
                            </thead>


                            <tbody>
                            <?php $a = 1 ?>
                            @foreach($invoice->payamounts as $payamount)
                                <tr>
                                    <td><?= $a ?></td>
                                    <td>{{\Carbon\Carbon::parse($payamount->paid_date)->format("d-m-Y")}}</td>

                                    <td>
                                        ${{$payamount->paid_amount}}
                                    </td>

                                    <td>
                                        <form action={{url('payamount/'.$payamount->id)}} method="POST">

                                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                                            <input type="hidden" name="_method" value="DELETE">

                                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are You Sure??')"><i class="fa fa-trash-o" title="Delete"></i></button>

                                        </form>
                                    </td>
                                </tr>
                                <?php $a = 1 + $a ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop


