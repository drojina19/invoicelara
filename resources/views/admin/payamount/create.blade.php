@extends('admin.baselayout.baselayout')
@section('main-content')

    {{--coded by sagar date:wed aug9--}}
    <div class="page-title">
        <div class="pull-left">
            <h3>Add Payment for {{$invoice->invoice_no}}</h3>
        </div>

        <div class="pull-right">
            <a href="{{url('payamount/'.$invoice->id)}}" class="btn btn-primary pull-right">List {{$invoice->invoice_no}} Payments</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form id="demo-form2" class="form-horizontal form-label-left" action="{{route('payamount.store')}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Pay Amount Detail</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Supplier Name</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="{{$invoice->supplier->supplier_name}}" disabled required>
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <input type="hidden" name="invoice_no" class="form-control" data-inputmask="'mask': '99/99/9999'" value="{{$invoice->invoice_no}}" >
                        <input type="hidden" name="invoice_id" class="form-control" data-inputmask="'mask': '99/99/9999'" value="{{$invoice->id}}" >
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Customer Name</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="{{$invoice->customer->name}}" disabled required>
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Amount Due</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" id="amt_owed"  data-inputmask="'mask': '99/99/9999'" value="${{$invoice->amt_owed}}" disabled required>
                                <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Paid Date *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="paid_date" id="paid_date" data-inputmask="'mask': '99/99/9999'" value="{{old('paid_date')}}" {{$invoice->amt_owed==0?"disabled":""}}  required>
                                <span class="fa fa-calendar-check-o form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Amount Paid *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control"  id="amt_paid" name="paid_amount" data-inputmask="'mask': '99/99/9999'" value="{{old('paid_amount')}}" {{$invoice->amt_owed==0?"disabled":""}} required>
                                <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">New Amount Due</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control"  id="newamt_owed" data-inputmask="'mask': '99/99/9999'" value="{{$invoice->amt_owed}}" disabled required>
                                <span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success" {{$invoice->amt_owed==0?"disabled":""}} >Submit</button>
                            <button type="reset" class="btn btn-default" {{$invoice->amt_owed==0?"disabled":""}} >Reset</button>
                            <a href="{{url('payamount/'.$invoice->id)}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
    @section('custom-scripts')
        <script src="{{asset('js/custom.js')}}"></script>
        <script>
            $('#paid_date').daterangepicker({
            locale: {
            format: 'DD-MM-YYYY'
            },
            singleDatePicker: true,
            singleClasses: "picker_3"
            }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            });
        </script>
    @endsection
@stop