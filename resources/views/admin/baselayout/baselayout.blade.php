<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{config('app.name')}}</title>

    @include('admin.partials.css')
    @yield('custom-css')
</head>

<body class="nav-md footer_fixed">
<div class="container body">
    <div class="main_container">

        <div class="row" style="border-bottom:1px solid #D9DEE4; margin: 0px">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 ">
                <a href="{{url('/')}}">
                    <img src="{{asset('images/logo/inv.JPG')}}" alt="{{\App\Constant\Constant::INVOICE_SYSTEM}}" class="img-responsive" style="width: 25%">
                </a>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                <img src="{{asset('images/logo/logo.png')}}" alt="{{\App\Constant\Constant::INVOICE_SYSTEM}}" class="img-responsive pull-right">
            </div>
            <div class="clearfix"></div>
        </div>
        {{--left navbar--}}
        @include('admin.partials.leftnav')

        <!-- top navigation -->
        @include('admin.partials.topnavbar')
        <!-- /top navigation -->
        <div class="right_col" role="main">
        <!-- page content -->
        @yield('main-content')
        <!-- /page content -->
        </div>

        <!-- footer content -->
        {{--<footer style="margin-top: 20px;">--}}
            {{--<div class="pull-right">--}}
                {{--{{config('app.name')}}--}}{{--- Develped By: <a href="http://geniusitsolutions.com.au/" target="_blank">Genius IT Solutions</a>--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</footer>--}}
        <!-- /footer content -->
    </div>
</div>

@include('admin.partials.scripts')
@yield('custom-scripts')
</body>
</html>