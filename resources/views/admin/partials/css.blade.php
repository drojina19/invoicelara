<!-- Bootstrap -->
<link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}"rel="stylesheet">
<!-- Font Awesome -->
<link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
<link href="{{asset('/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
<!-- Custom styling plus plugins -->
<link href="{{asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('build/css/custom.css')}}" rel="stylesheet">

