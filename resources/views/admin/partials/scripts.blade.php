<!-- jQuery -->
<script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('moment/moment.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('magnific-popup/jquery.magnific-popup.min.js')}}"></script>



<!-- Custom Theme Scripts -->
<script src="{{asset('build/js/custom.js')}}"></script>

<script>
    $(document).ready(function(){
        


    });
</script>

@yield('custom-script')
