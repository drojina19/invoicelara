<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{asset('images/logo/user.png')}}" alt="{{Auth::user()->name}}" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Auth::user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        {{--<br />--}}

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard </a>
                    </li>
                    <li><a><i class="fa fa-user-plus"></i>Suppliers <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('supplier.create')}}">Add Supplier</a></li>
                            <li><a href="{{route('supplier.index')}}">List Suppliers</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-user"></i>Customers <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('customer.create')}}">Add Customer</a></li>
                            <li><a href="{{route('customer.index')}}">List Customers</a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-product-hunt"></i>Products <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('product.create')}}">Add products</a></li>
                            <li><a href="{{route('product.index')}}">List Products</a></li>
                        </ul>
                    </li>
                    {{--<li><a><i class="fa fa-money"></i>Payment <span class="fa fa-chevron-down"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a href="{{route('payment.create')}}">Add Payment</a></li>--}}
                            {{--<li><a href="{{route('payment.index')}}">List Payment</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    <li> <a><i class="fa fa-file-excel-o"> </i>Invoice <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('invoice.create')}}"> Add Invoice</a></li>
                            <li><a href="{{route('invoice.index')}}">List Invoice</a></li>

                        </ul>

                    </li>


                    <li><a><i class="fa fa-trash"></i>Trash <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{url('deletedcustomers')}}">Deleted Customers</a></li>
                            <li><a href="{{url('deletedproducts')}}">Deleted Products</a></li>
                            <li><a href="{{url('deletedinvoices')}}">Deleted Invoice</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('/changepassword')}}"><i class="fa fa-key"></i>Change Password</a>
                    </li>

                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>