<div class="top_nav">


    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="{{ url('/logout')}}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i>
                        <span class="hidden-xs">
                                Logout
                        </span>
                    </a>
                    <form id="logout-form" action="{{ url('/logout')}}" method="POST" style="display: none;">
                        {{ csrf_field()}}
                    </form>

                </li>

            </ul>
        </nav>
    </div>
</div>