@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="pull-left">
            <h3>Deleted Payment Method</h3>
        </div>




        <div class="pull-right">
            <a href="{{url('/payment/'.$supplier->id)}}" class="btn btn-primary pull-right">List Payment Methods</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

    @if(\Illuminate\Support\Facades\Session::has('payment'))
        <div class="col-md-12" id="status">

            <div class="alert alert-success text-center">
                {{\Illuminate\Support\Facades\Session::get('payment')}}
            </div>

        </div>

    @endif


    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Supplier: {{$supplier->supplier_name}}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>

                                <th> Bank Name</th>
                                <th> BSB</th>
                                <th>Account Number</th>
                                <th>Action(s)</th>

                            </tr>
                            </thead>


                            <tbody>
                            <?php $a = 1 ?>
                            @foreach($payments as $payment)
                                <tr>
                                    <td><?= $a ?></td>

                                    <td>{{$payment->bank_name}}</td>
                                    <td>{{$payment->bsb}}</td>
                                    <td>{{$payment->account_no}}</td>
                                    <td>
                                        <a href="{{url('/restorepayment/'.$payment->id)}}" title="Restore Payment Method" class="btn btn-xs btn-default"> <i class="fa fa-undo"> </i></a>
                                        <a href="{{url('/forcedeletepaymtd/'.$payment->id)}}" title="Delete Permanently" class="btn btn-xs btn-danger" onclick="return confirm('Are You Sure??')"> <i class="fa fa-trash-o"> </i></a>
                                    </td>

                                </tr>
                                <?php $a = 1 + $a ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop