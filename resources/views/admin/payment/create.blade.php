@extends('admin.baselayout.baselayout')
@section('main-content')


    <div class="page-title">
        <div class="title_left">
            <h3>Add New Payment Method</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(\Illuminate\Support\Facades\Session::has('payment'))
        <div class="alert alert-danger" id="status">
            {{\Illuminate\Support\Facades\Session::get('payment')}}
        </div>
    @endif

    <form class="form-horizontal form-label-left" action="{{route('payment.store')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Payment Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <input type="hidden" class="form-control" name="supplier_id" value="{{$supplier->id}}">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Supplier Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="supplier_name" data-inputmask="'mask': '99/99/9999'" disabled value="{{$supplier->supplier_name}}">
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Bank Name *:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="bank_name" data-inputmask="'mask': '99/99/9999'" value="{{old('bank_name')}}" required>
                                <span class="fa fa-bank form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">BSB *:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="bsb" data-inputmask="'mask': '99/99/9999'" value="{{old('bsb')}}" required>
                                <span class="fa fa-info form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Account Number *:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="account_no" data-inputmask="'mask': '99/99/9999'"  value="{{old('account_no')}}"required>
                                <span class="fa fa-book form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Payment Method *:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="pay_method" data-inputmask="'mask': '99/99/9999'" value="{{old('pay_method')}}" required>
                                <span class="fa fa-external-link form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>



                    </div>
                </div>
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-9 col-xs-12 " align="center">
                            <button type="submit" class="btn btn-success">Submit</button>

                            <button type="reset" class="btn btn-default">Reset</button>

                            <a href="{{url('/payment/'.$supplier->id)}}" class="btn btn-danger">Cancel</a>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </form>



@stop