@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="pull-left">
            <h3>Payment Method</h3>
        </div>




        <div class="pull-right">
            <a href="{{url('/payment/add/'.$supplier->id)}}" class="btn btn-primary pull-right">Add Payment Method</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

        @if(\Illuminate\Support\Facades\Session::has('payment'))
        <div class="col-md-12" id="status">

            <div class="alert alert-success text-center">
                {{\Illuminate\Support\Facades\Session::get('payment')}}
            </div>

        </div>

        @endif


    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Supplier: {{$supplier->supplier_name}}</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>

                                <th>Bank Name</th>
                                <th>BSB</th>
                                <th>Account Number</th>
                                <th>Pay Method</th>
                                <th>Status</th>
                                <th>Action(s)</th>

                            </tr>
                            </thead>


                            <tbody>
                            <?php $a = 1 ?>
                            @foreach($payments as $payment)
                                <tr>
                                    <td><?= $a ?></td>

                                    <td>{{$payment->bank_name}}</td>
                                    <td>{{$payment->bsb}}</td>
                                    <td>{{$payment->account_no}}</td>
                                    <td>{{$payment->pay_method}}</td>
                                    <td>
                                        @if($payment->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>

                                    <td>
                                        <a href="{{url('payment/'.$payment->id.'/edit')}}" title="Edit" class="btn btn-xs btn-info pull-left"> <i class="fa fa-pencil"> </i></a>
                                        @if($payment->status==0)
                                            <form action="{{url('payment/'.$payment->id)}}" method="POST" class="pull-left">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Are You Sure??')"><i class="fa fa-trash-o" title="Delete"></i></button>
                                            </form>
                                        @endif
                                        @if($payment->status==0)
                                            <a href="{{url('payment/mtdenable/'.$payment->id)}}" class="btn btn-xs btn-primary pull-left"><i class="fa fa-check" title="Enable Payment Method"></i></a>
                                        @endif
                                        <div class="clearfix"></div>
                                    </td>
                                    <div class="clearfix"></div>
                                </tr>
                                <?php $a = 1 + $a ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


@stop