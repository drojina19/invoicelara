@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="pull-left">
            <h3>  List Products</h3>
        </div>




        <div class="pull-right">
            <a href="{{url('/product/create')}}" class="btn btn-primary pull-right">Add New Product</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

    @if(Session::has('product'))
        <div class="col-md-12" id="status">
            <div class="alert alert-success text-center">
                {{Session::get('product')}}
            </div>

        </div>

    @endif


    <div class="">


        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        {{--<h2>Supplier: {{$supplier->supplier_name}}</h2>--}}
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable-product" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>

                                <th> Product Name</th>
                               
                                <th>Status</th>
                                <th>Action(s)</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $a = 1 ?>
                            @foreach($products as $product)
                                <tr>
                                    <td><?= $a ?></td>

                                    <td>{{$product->name}}</td>
                                    
                                    <td>
                                        @if($product->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>


                                    <td>
                                        <a href="{{url('product/'.$product->id.'/edit')}}" title="Edit" class="btn btn-xs btn-info pull-left"> <i class="fa fa-edit"> </i></a>
                                        @if($product->status==0)
                                            @if(count($product->invoiceDetails)==0)
                                            <form action="{{url('product/'.$product->id)}}" method="POST" class="pull-left">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('{{\App\Constant\Constant::DELETE_SOFT}}')"><i class="fa fa-trash-o" title="Delete"></i></button>
                                            </form>
                                            @endif
                                        @endif
                                        @if($product->status==0)
                                            <a href="{{url('product/p-enable/'.$product->id)}}" class="btn btn-xs btn-primary pull-left" onclick="return confirm('{{\App\Constant\Constant::ACTIVATE}}')"><i class="fa fa-check" title="Enable product Method"></i></a>
                                        @endif
                                        @if ($product->status==1)
                                            <a href="{{url('product/p-disable/'.$product->id)}}" class="btn btn-xs btn-warning pull-left"  onclick="return confirm('{{\App\Constant\Constant::DE_ACTIVATE}}')"> <i class="fa fa-warning" title="Disable product Method"></i></a>
                                        @endif

                                        <div class="clearfix"></div>
                                    </td>
                                    <div class="clearfix"></div>
                                </tr>
                                <?php $a = 1 + $a ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@section('custom-scripts')
    <script>
        $("#datatable-product").dataTable({});

    </script>
@endsection


@stop