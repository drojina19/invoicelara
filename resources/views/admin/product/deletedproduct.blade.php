@extends('admin.baselayout.baselayout')
@section('main-content')
    

    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>List Deleted Product</h3>
            </div>

            <div class="pull-right ">
                <a href="{{route('product.index')}}" class="btn btn-primary pull-right">List products</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>

        <div class="clearfix"></div>
        @if(\Illuminate\Support\Facades\Session::has('product'))
            <div class="alert alert-success text-center col-md-12" id="status">
                {{\Illuminate\Support\Facades\Session::get('product')}}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All products</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>

                                <th>Status</th>
                                <th>Action(s)</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->name}}</td>

                                    <td>
                                        @if($product->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{url('restoreproduct/'.$product->id)}}" class="btn btn-xs btn-default pull-left" title="Restore product" onclick="return confirm('{{\App\Constant\Constant::RESTORE_ITEM}}')"><i class="fa fa-undo"></i></a>
                                        <form action="{{url('forcedeletedproduct/'.$product->id)}}" method="POST" class="pull-left">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('{{\App\Constant\Constant::DELETE_PERMANENT}}')"><i class="fa fa-trash-o" title="Delete"></i></button>
                                        </form>
                                        <div class="clearfix"></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



@stop