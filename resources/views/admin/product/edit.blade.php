@extends('admin.baselayout.baselayout')
@section('main-content')


    <div class="page-title">
        <div class="pull-left">
            <h3>Edit Product</h3>
        </div>

        <div class="pull-right">
            <a href="{{route('product.index')}}" class="btn btn-primary pull-right">List Products</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-horizontal form-label-left" action="{{route('product.update',['id'=>$product->id])}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">

                    <div class="x_content">
                        <br />


                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Product(s)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr >

                                <td>{{$product->id}}</td>
                                <td >
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12 product_name">
                                            <input type="text" class="form-control" name="name" data-inputmask="'mask': '99/99/9999'" value="{{$product->name}}" required>
                                            <span class="fa fa-product-hunt form-control-feedback right" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                            </tbody>
                        </table>

                    </div>
                </div>



                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                            <button type="submit" class="btn btn-success">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <a href="{{route('product.index')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
@section('custom-script')
    <script src="{{asset('js/custom.js')}}"></script>
@stop
@stop
