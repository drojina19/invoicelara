@extends('admin.baselayout.baselayout')
@section('main-content')

    {{--coded by:rojina date:9th August--}}
    <div class="">
        <div class="page-title">
            <div class="pull-left">
                <h3>Supplier</h3>
            </div>

            <div class="pull-right">
                <a href="{{route('supplier.create')}}" class="btn btn-primary pull-right">Add Supplier</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                @if(\Illuminate\Support\Facades\Session::has('supplier'))
                    <div class="alert alert-success text-center" id="status">
                        {{\Illuminate\Support\Facades\Session::get('supplier')}}
                    </div>
                @endif
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Suppliers</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>SN</th>

                                <th>Supplier Name</th>
                                <th> Address</th>
                                <th  style="width: 10%">Logo</th>
                                <th>Status</th>
                                <th>Action(s)</th>
                                <th>Payment Method</th>
                            </tr>
                            </thead>


                            <tbody>
                            <?php $a = 1 ?>
                            @foreach($suppliers as $supplier)
                                <tr>
                                    <td><?= $a ?></td>
                                    <td>{{$supplier->supplier_name}}</td>
                                    <td>{{$supplier->street}} {{$supplier->city}} {{$supplier->state}} {{$supplier->country}}</td>
                                    <td><img src="{{asset('images/supplier/'.$supplier->logo)}}" class="img-responsive img"></td>
                                    <td>
                                        @if($supplier->status==1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Inactive</span>
                                        @endif
                                    </td>
                                    <td>

                                        <a href="{{url('/supplier/'.\Illuminate\Support\Str::slug($supplier->id))}}" class="btn btn-default btn-xs"><i class="fa fa-search-plus" title="View Detail"></i></a>
                                        <a href="{{url('supplier/'.$supplier->id.'/edit')}}" class="btn btn-info btn-xs" title="Edit"> <i class="fa fa-edit"></i></a>

                                    </td>

                                    <td>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{url('/payment/add/'.$supplier->id)}}" title="Add Payment Method" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a>
                                                <a href="{{url('/payment/'.$supplier->id)}}" title="List Payment Method" class="btn btn-success btn-xs"><i class="fa fa-table"></i></a>

                                            </div>
                                        </div>




                                    </td>
                                </tr>
                                <?php $a = 1 + $a ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
 @stop


