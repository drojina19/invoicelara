@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="pull-left">
            <h3>Edit Supplier</h3>
        </div>

        <div class="pull-right">
            <a href="{{route('supplier.index')}}" class="btn btn-primary pull-right">List Suppliers</a>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif
    <form class="form-horizontal form-label-left" action="{{route('supplier.update',[$supplier->id])}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Supplier's Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Supplier Name *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="supplier_name" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->supplier_name}}" required>
                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ABN *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="abn" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->abn}}" required>
                                <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Website</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="website" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->website}}">
                                <span class="fa fa-globe form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Logo </label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="file" class="form-control" name="logo" id="logo" data-inputmask="'mask': '99/99/9999'" >
                                <span class="fa fa-image form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contact Details</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Email *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="email" class="form-control" name="email" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->email}}" required>
                                <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 1 *</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone1" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->phone1}}" required >
                                <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 2</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <input type="text" class="form-control" name="phone2" data-inputmask="'mask': '99/99/9999'" value="{{$supplier->phone2}}">
                                <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Address</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Street Address *</label>
                            <input type="text" class="form-control" name="street" id="inputSuccess3" value="{{$supplier->street}}"  required>
                            <span class="fa fa-map-marker form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">City *</label>
                            <input type="text" class="form-control" name="city" id="inputSuccess3" value="{{$supplier->city}}" required>
                            <span class="fa fa-location-arrow form-control-feedback right" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="state">State *</label>
                            <select class="form-control" name="state" required>
                                <option value="NSW" {{$supplier->state=="NSW"?'selected':''}}>NSW</option>
                                <option value="VIC" {{$supplier->state=="VIC"?'selected':''}}>VIC</option>
                                <option value="QLD" {{$supplier->state=="QLD"?'selected':''}}>QLD</option>
                                <option value="ACT" {{$supplier->state=="ACT"?'selected':''}}>ACT</option>
                                <option value="SA" {{$supplier->state=="SA"?'selected':''}}>SA</option>
                                <option value="NT" {{$supplier->state=="NT"?'selected':''}}>NT</option>
                                <option value="WA" {{$supplier->state=="WA"?'selected':''}}>WA</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Zip Code *</label>
                            <input type="text" class="form-control" name="zip_code" id="zip_code" value="{{$supplier->zip_code}}" required>
                            <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="country">Country *</label>
                            <select class="form-control" name="country" required>
                                <option value="Australia" {{$supplier->country=='Australia'?'selected':''}}>Australia</option>
                                <option value="Nepal" {{$supplier->country=='Nepal'?'selected':''}}>Nepal</option>
                            </select>
                        </div>




                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12" align="center">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                        <a href="{{route('supplier.index')}}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </div>
        </div>

    </form>
@stop