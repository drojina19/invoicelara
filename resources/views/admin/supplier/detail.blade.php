@extends('admin.baselayout.baselayout')
@section('main-content')
    <div class="page-title">
        <div class="page-title col-md-12">
            <div class="pull-left">
                <h3>{{ucfirst($supplier->supplier_name)}}</h3>
            </div>

            <div class="pull-right">
                <a href="{{route('supplier.index')}}" class="btn btn-primary pull-right">List Suppliers</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>



        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Supplier's Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Supplier Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$supplier->supplier_name}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">ABN:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$supplier->abn}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Website:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$supplier->website}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6"> Logo: </label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                <img src="{{asset('images/supplier/'.$supplier->logo)}}" alt="{{$supplier->supplier_name}}" class="img img-responsive" style="max-width: 200px; border: 1px solid #a5a5a5">
                            </div>
                        </div> </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Contact Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Email:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$supplier->email}}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 1:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6">
                                {{$supplier->phone1}}
                            </div>
                        </div>
                    </div>

                   <div class="row">
                       <div class="form-group">
                           <label class="control-label col-md-4 col-sm-6 col-xs-6">Phone Number 2:</label>
                           <div class="col-md-8 col-sm-8 col-xs-6">
                               {{$supplier->phone2==null?'-':$supplier->phone2}}
                           </div>
                       </div>
                   </div>

                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Address</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Street Address: </label>
                            {{$supplier->street}}
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">City</label>
                            {{$supplier->city}}
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="state">State: </label>
                            {{$supplier->state}}

                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="">Zip Code: </label>
                            {{$supplier->zip_code}}
                        </div>

                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                            <label for="country">Country:</label>
                            {{$supplier->country}}

                        </div>
                </div>
            </div>

            {{-- <div class="col-md-12 col-sm-9 col-xs-12 " align="center">

                <a href="" class="btn btn-default">View Invoices</a>

            </div> --}}

    </div>

    </div>
@stop